## This script is to help build data base needed for the extractor of 9x9 vain
from imageReader import imageReader
import os
import cv2

def fileRead():
    path = "C:\\study\\study\\memory"
    files = []
    for r, d, f in os.walk(path):
        for file in f:
            files.append(os.path.join(r, file))
    return files
refPt = [0, 0]

def click_and_crop(event, x, y, flags, param):
    global refPt
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [x, y]
    else:
        refPt = [0, 0]

def extractSquer9(image):
    global refPt
    color = (256, 0, 0)
    thickness = -1
    clone = image.copy()
    cv2.namedWindow("image")
    cv2.setMouseCallback("image", click_and_crop)
    # keep looping until the 'q' key is pressed
    while True:
        # display the image and wait for a keypress
        cv2.imshow("image", image)
        x = cv2.waitKey(1) & 0xFF
        print("vein")
        if(refPt[0]>0 and refPt[1]>0):
            cv2.imshow("new",image[refPt[0]-1:refPt[0]+1,refPt[1]-1:refPt[1]+1])


def main():
    files = fileRead()
    for file in files:
        image = imageReader(file, None)
        image.convertRawToImage()
        extractSquer9(image.imageArray)
        cv2.imshow("new",image.imageArray)

if __name__ == '__main__':
    main()

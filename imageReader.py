

import cv2
import numpy as np
import matplotlib.pyplot as plt
import platform
from filter import wavelate

def saveImage(image,path):
    cv2.imwrite(image,path)

def nameExtractor(path):
    if (platform.system() == "Windows"):
        stringList = str.split(path, "\\")
    else:
        path = str.split(path, "data/")[1]
        stringList = str.split(path, "/")
    name = stringList[1] + stringList[len(stringList) - 1]
    name = name.replace(".txt","")
    print(stringList)
    state = "sick" if stringList[0].count("sick") == 1 else "healthy"
    return name,state

class imageReader():
    def __init__(self, path, image, sorfVal=[], mask = [],debug = False):
        self.path = path
        self.imageFileRaw = []
        self.imageDnoise= [] # image Array contain a normal image after
        self.imageValue = [] # image_value contain the value of the image without any change.
        if image is not None:
            self.imageValue = image
            rgb_out = np.zeros(self.imageValue.shape, np.uint8)
            self.imageDnoise = cv2.normalize(self.imageValue, rgb_out, 0, 255,
                                            cv2.NORM_MINMAX).astype(np.uint8)
        self.body = []
        self.bin = []
        self.sorfVal = sorfVal
        self.mask = mask
        self.debug = debug


    def showImage(self,img):
        cv2.destroyAllWindows()
        cv2.imshow("new", img)
        cv2.waitKey(0)

    def convertRawToImage(self):
        '''
        Data is presented as a text. I am interst to extrcat the tempture from the txt.
        The data is in a form of > 2^14 an so we will need some HDR component to extract it.
        :return: 
        '''
        self.imageFileRaw = np.loadtxt(self.path) # The tempture
        self.imageDnoise = self.imageFileRaw
        rgb_out = np.zeros(self.imageDnoise.shape, np.uint8)
        rgb_out = cv2.normalize(self.imageDnoise, rgb_out, 0, 1,
                                cv2.NORM_MINMAX).astype(float)
        NlMeans = wavelate(rgb_out,self.debug)

        if (self.debug):
            out = cv2.hconcat([rgb_out,NlMeans])
            self.showImage(out)
            print("michael")

        self.imageValue = rgb_out
        self.imageDnoise = NlMeans

    def findBackGroundAndCollectPoint(self, name1, img):
        print(self.path)

        name,state = nameExtractor(self.path)

        fig, ((ax, ax1), (ax2, ax3)) = plt.subplots(2, 2)
        fig.suptitle(name + state + ".jpg", fontsize=20)
        mask = (img > 50).astype(np.uint8())
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        img = np.array(img).astype(np.uint8)
        img = clahe.apply(img)
        self.body = cv2.bitwise_and(img, img, mask=mask)
        self.bin = cv2.adaptiveThreshold(self.body, 255,
                                         cv2.ADAPTIVE_THRESH_MEAN_C,
                                         cv2.THRESH_BINARY, blockSize=5, C=0)
        if self.debug:
            ax.imshow(img)
            ax1.imshow(self.bin)
        self.bin = cv2.medianBlur(self.bin, 5)
        if self.debug:
            ax2.imshow(self.bin)
        rgb = cv2.cvtColor(self.body, cv2.COLOR_GRAY2RGB)
        matrix = 255 * np.ones(rgb[:, :, 2].shape).astype(np.uint8)
        rgb[:, :, 0] = cv2.bitwise_and(matrix, self.bin) + cv2.bitwise_and(rgb[
                                                                           :, :,
                                                                           0],
                                                                           cv2.bitwise_not(
                                                                               self.bin))
        rgb[:, :, 1] = cv2.bitwise_and(rgb[:, :, 1], cv2.bitwise_not(self.bin))
        rgb[:, :, 2] = cv2.bitwise_and(rgb[:, :, 2], cv2.bitwise_not(self.bin))
        if False:
            ax3.imshow(rgb)
        fig.savefig("./output2/"+name + state + str(name1) + "_method2.jpg")
        fig1, ax3 = plt.subplots(1, 2)
        fig1.suptitle(name + state + "original.jpg", fontsize=20)
        if self.debug:
            ax3[0].imshow(self.imageValue)
            ax3[1].imshow(self.imageDnoise)
        fig1.savefig("./output2/"+name + state + str(name1) + "original.jpg")
        if self.sorfVal!=[]:
            cv2.bitwise_and(img, img,self.mask)
            img_rgb = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            img_rgb = cv2.drawContours(img_rgb, self.sorfVal.border[0], -1,
                                             (0, 0, 255),
                                             thickness=2)
            if 0:
                img_rgb = cv2.drawContours(img_rgb, self.sorfVal.border_comp[0], -1,
                                           (0, 255, 0),
                                           thickness=2)
            plt.figure()
            plt.imshow(img_rgb)
            plt.suptitle(name + state + ".jpg", fontsize=20)
            plt.savefig("./output1/"+name + state + str(name1) + "sorf.jpg")

    def getNormilized(self,image):
        return cv2.normalize(image, dst=None, alpha=0, beta=1,
                             norm_type=cv2.NORM_MINMAX).astype(float)

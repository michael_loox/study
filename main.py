import os
import hrdLabs as hdr
from imageReader import imageReader
import matplotlib.pyplot as plt
import SorfProcessing as SP
import numpy as np
import cv2
from extractor import extractor
from imageReader import nameExtractor
import levelSet1

debug = False


def saveImage(image,path):
    if image is not None:
        cv2.imwrite(str(path),image*255)


def main():
    files = []
    path = "./data/sick"
    path = "./data/healthy"
    path = "./data/"
    saveNipplePath = "./nippleFirstSign"

    mode = 'hdr'
    for r, d, f in os.walk(path):
        for file in f:
            if 'PAC_51' in file:
                files.append(os.path.join(r, file))
                #if 'DN10.txt' in file:


    for file in files:
        for i in range(1,2):
            val = None
            if i==1:
                val,hdrRuner = hdr.runHdrAlgoritham(file,debug)
                originalFile = hdrRuner.getNormilized(hdrRuner.imageValue)
                originalDnoise = hdrRuner.getNormilized(hdrRuner.imageDnoise)
                hdrImage = hdrRuner.getNormilized(val.result)

                sorfNippleSort = SP.SorfProcessingClass(val.imageArray,
                                                        debug=debug)
                sorfNippleSort.SorfProcessing('gray', 'gaussian', 'gaussian',
                                              'SORF', -6, [])
                name, state = nameExtractor(file)
                ext = extractor(hdrRuner.imageFileRaw,hdrImage,file)

                if debug:
                    saveImage(originalFile, str(saveNipplePath) + "/" + str(state) + str(name) + "original.jpg")
                    saveImage(originalDnoise, str(saveNipplePath) + "/" + str(state) + str(name) + "originalDnoise.jpg")
                    saveImage(hdrImage, str(saveNipplePath) + "/" + str(state) + str(name) + "hdr.jpg")
                    ext.showImage(originalFile)
                    ext.showImage(val.result)

                # All body
                body,mask,resultR,labelsR,resultL,labelsL = ext.connected_component_label(val.path)
                # Rigth Side
                sorf1 = SP.SorfProcessingClass(resultR, labelsR, debug=debug)
                sorf1.SorfProcessing('gray', 'gaussian', 'gaussian', 'SORF', -10,
                                    [])
                valE,r = ext.findNippleAndCrop(sorf1.border,resultR*labelsR)
                if r!=None:
                    saveImage(valE, str(saveNipplePath)+"/"+str(r)+"/"+ str(state) + str(name) + "R.jpg")

                # Left Side
                sorf2 = SP.SorfProcessingClass(resultL, labelsL, debug=debug)
                sorf2.SorfProcessing('gray', 'gaussian', 'gaussian', 'SORF', -10,
                                    [])

                valE,r = ext.findNippleAndCrop(sorf2.border, resultL*labelsL)
                if r!=None:
                    saveImage(valE, str(saveNipplePath)+"/"+str(r)+"/" + str(state) + str(name) + "L.jpg")

                sorf = SP.SorfProcessingClass(body, mask,debug=debug)
                sorf.SorfProcessing('gray','gaussian','gaussian','SORF',3,[])
                #ext.showImage(sorf.contours1)
                continer = imageReader(file,val.result,sorf,mask)

            elif(i==2 and mode == "compare"):
                continer = imageReader(file, None)
            if val is None:
                continer.convertRawToImage()
            continer.findBackGroundAndCollectPoint(i, continer.imageDnoise)
    
    
    
if __name__ == "__main__":
    main()
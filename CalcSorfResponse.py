import numpy as np
import cv2
class CenSizesClass:

    def __init__(self):
        self.Bmode = [3, 5, 7, 10, 12, 15, 18, 20]
        self.Elasto = [1, 3, 5]
        self.ElastoInten = [12, 15, 18]


class AlgParamsSet:

    def __init__(self , image):
        self.LocalSize = 10
        self.LocalDecayCoef = 10
        self.RemoteSize = 30
        self.RemoteDecayCoef = 30
        self.ColorImageFlag = 0
        self.FieldSizeFactor = 3
        self.CenSizes = CenSizesClass()
        self.InputImgBmode = image
        self.SrndDecayCoefFactor = 2
        self.CenDecayCoefFactor = 1
        self.MultiResPowerBmode = 2
        self.ShowBmodeRes = False

def CalcFieldMask(FieldSize, DecayCoef, CenSize, TypeOfFltr):
    '''initialize mask with defualt values, compatible for "average" filter type'''
    FieldMask = np.ones([FieldSize, FieldSize]);
    if (TypeOfFltr == "gaussian"):
        for i in range(1, FieldSize):
            for j in range(1, FieldSize):
                FieldMask[i, j] = np.exp(-((i - (FieldSize + 1) / 2) ** 2 + (j - (FieldSize + 1) / 2) ** 2) / DecayCoef ** 2)
    ''' put zeros in center region if needed'''
    if (CenSize != 0):
        ''' put zeros in the position of the center region of the rceptive field'''
        FieldMask[int(np.round((FieldSize - CenSize + 2) / 2)): int(np.round((FieldSize + CenSize) / 2)), int(np.round(
            (FieldSize - CenSize + 2) / 2)): int(np.round((FieldSize + CenSize) / 2))] = 0
    FieldMask = FieldMask / np.sum(FieldMask);
    return FieldMask

def CalcSorfResponse(AlgParams, InputImg, CenSize, TypeOfCenFltr, TypeOfSrndFltr, TypeOfOperation):

    # size of receptive field
    FieldSize = AlgParams.FieldSizeFactor * CenSize;

    # decay coefficient of surround mask
    SrndDecayCoef = AlgParams.SrndDecayCoefFactor * CenSize;

    # decay coefficient of center mask
    CenDecayCoef = AlgParams.CenDecayCoefFactor * CenSize;

    # calculate center and surround masks
    CenterMask = CalcFieldMask(CenSize, CenDecayCoef, 0, TypeOfCenFltr);
    SurroundMask = CalcFieldMask(FieldSize, SrndDecayCoef, CenSize, TypeOfSrndFltr);


    # the lumanance of a gray image is the image itself
    LumImg = np.double(InputImg)

    # caculate SORF of relative response on luminance image
    if TypeOfOperation =='SORF':  # SORF response
    # center response
        CenResponse = cv2.filter2D(LumImg,cv2.CV_64F, CenterMask)
        ##CenResponse = imfilter(LumImg, CenterMask, 'symmetric', 'same');

        # surround response
        SrndResponse = cv2.filter2D(LumImg,cv2.CV_64F, SurroundMask)
        ##SrndResponse = imfilter(LumImg, SurroundMask, 'symmetric', 'same');

            # center response minus surround response
        return CenResponse - SrndResponse

    elif TypeOfOperation == 'relative':  # Relative response (center divided by surround)
        # center response
        CenResponse = cv2.filter2D(LumImg,cv2.CV_64F, CenterMask)
        #CenResponse = imfilter(LumImg, CenterMask, 'symmetric', 'same');

        # surround response
        SrndResponse = cv2.filter2D(LumImg,cv2.CV_64F,SurroundMask)
        #SrndResponse = imfilter(LumImg, SurroundMask, 'symmetric', 'same');
        # SrndResponse(SrndResponse<=1e-3) = 0;

        # put zero in places surrounded by zero
        CenResponse[SrndResponse == 0] = 0;

        # replace zeros with a small number, to avoid division in zero
        SrndResponse[SrndResponse == 0] = 1e-1;

        # center response divided by surround response
        return  CenResponse / SrndResponse;
    else:  # error
        assert('Wrong type of operation', 'Function "CalcSorfResponse"')

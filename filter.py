import matplotlib.pyplot as plt
from skimage.restoration import (denoise_wavelet, estimate_sigma)
from skimage import data, img_as_float
from skimage.util import random_noise
from skimage.metrics import peak_signal_noise_ratio
import  numpy as np

def wavelate(noisy,debug=False):
    if debug:
        fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 5),
                               sharex=True, sharey=True)
        plt.gray()

    original = np.copy(noisy)


    # Estimate the average noise standard deviation across color channels.
    sigma_est = estimate_sigma(noisy, average_sigmas=True)
    # Due to clipping in random_noise, the estimate will be a bit smaller than the
    # specified sigma.
    print(f'Estimated Gaussian noise standard deviation = {sigma_est}')

    im_bayes = denoise_wavelet(noisy, convert2ycbcr=False,
                               method='BayesShrink', mode='soft',wavelet='db1',
                               rescale_sigma=True,wavelet_levels=5,sigma=sigma_est)

    # Compute PSNR as an indication of image quality


    if debug:
        # Compute PSNR as an indication of image quality
        psnr_noisy = peak_signal_noise_ratio(original, noisy, data_range=1)
        psnr_bayes = peak_signal_noise_ratio(original, im_bayes,data_range=1)
        ax[0].imshow(noisy)
        ax[0].axis('off')
        ax[0].set_title(f'Noisy\nPSNR={psnr_noisy:0.4g}')
        ax[1].imshow(im_bayes)
        ax[1].axis('off')
        ax[1].set_title(
            f'Wavelet denoising\n(BayesShrink)\nPSNR={psnr_bayes:0.4g}')


        fig.tight_layout()
        im_bayes.tofile('info.txt',"%f")
        plt.show()
    return im_bayes

'''
HDR
surfing algoritham <-->
graph cut <--> compleat line
'''
from imageReader import imageReader
import cv2
import numpy as np
import os
import matplotlib.pyplot as plt
import warnings
import time
#todo: To change accurding to original image.

warnings.filterwarnings('ignore')
class hdr:
    def __init__(self, path = "", imageArray = [],debug=False,Pyram=[],mode = "time"):
        self.hdrRuner = []
        self.path = path
        self.gamma = 1.0/2.2
        self.imageArray = imageArray
        self.GDBContainer = Pyram
        self.mode = mode
        if mode == "single":
            self.GDBContainer = []
        self.ContrastPyramid=[]   # The ration between the image to expand varion
        # todo: replace the method of the mask with minimum value of the body
        # tempture.
        # todo: replace uint8 to hdr values (not 255 rather 2^14)
        if mode == "time":
            self.mask = []
            self.body = []
        else:
            self.mask = (imageArray > 50).astype(np.uint8())
            self.body = self.NormAndConvertImagetoDouble(imageArray)#cv2.bitwise_and(imageArray,
        # imageArray,
                                               #mask=self.mask)
        self.numberOfLevel = 8
        self.SorfPyramid = [None] *self.numberOfLevel  # diff between with absult between the image to the expand version
        self.result =0
        if(debug):
            plt.gray()
        self.deltaSmall = []
        self.ResponsePyramid = []
        self.Rmax = np.ones(self.numberOfLevel)
        self.alpha = 1
        self.b = np.zeros(self.numberOfLevel)
        self.beta = 1
        self.lowerLimit = 0.5
        self.upperLimit = 5
        self.p = 1.2
        self.lmb = [2.94,2,1.8,1,0.4,0.3,0.3,0.3]
        self.GammaMin = 0.7
        self.GammaPyrmaid = []
        self.debug = debug
        self.Wn = [0.7,0.82,0.91,0.79,0.2,0.2,0.2,0.2]
        self.mu = np.ones([8]) \
            #[1,0.5,1/4,1/8,1/16,1/32,1/64,1/128]
        #self.Wn = [1,0.5,1/4,1/8,1/16,1/32,1/64,1/128]

    def __del__(self):
        self.Pyram = []
        print("Destructor called")

    def GDB(self):
        self.GDBContainer.append(np.array(self.body))
        j=0
        i=0
        if self.debug:

            fig, ax = plt.subplots(2, 4)
        while i < self.numberOfLevel:
            self.GDBContainer.append(cv2.pyrDown(self.GDBContainer[i])+1e-9)
            if self.debug:
                ax[j][i%4].imshow(self.GDBContainer[i])
                ax[j][i%4].set_title("layer: {}".format(i))
            if i%4==3:
                j+=1
            i+=1
        if self.debug:
            fig.canvas.set_window_title("GDB")
            fig.suptitle("Gaussian Pyramid Decomposition")
            fig.show()


            plt.figure()
            plt.imshow(self.imageArray)
            plt.show(block=False)

    def ContrastPyr(self):
        if self.debug:
            fig, ax = plt.subplots(2, 4)
        j = 0
        i = 0
        for i in range(0,self.numberOfLevel):
            Ip = self.GDBContainer[i]
            Iexpand = self.GDBContainer[i+1]
            if (self.mode == "single"):
                size = (self.GDBContainer[i].shape[1],
                        self.GDBContainer[i].shape[0])
                Iexpand = cv2.pyrUp(Iexpand, dstsize=size) + 1e-6
                C = Ip / Iexpand
            else:
                Iexpand = Iexpand + 1e-6
                C = Ip / Iexpand
            self.ContrastPyramid.append(C)
            if self.debug:
                ax[j][i % 4].imshow(self.ContrastPyramid[i])
                ax[j][i % 4].set_title("layer: {}".format(i))
            if i % 4 == 3:
                j += 1
            i += 1
        if self.debug:
            fig.suptitle("Contrast Pyramid")
            fig.canvas.set_window_title("Contrast Pyramid")
            fig.show()

    def NormAndConvertImagetoDouble(self,Im):
        NewImage = Im.astype(float) - np.amin(Im.astype(float))
        NewImage = NewImage / np.amax(NewImage)
        return NewImage

    def GenerateSorfPyramid(self):
        CenterSrndPyramid = []
        if self.debug:
            fig, ax = plt.subplots(2, 4)
        j = 0
        for i in range(0,self.numberOfLevel):
            Ip = self.GDBContainer[i]
            Iexpand = self.GDBContainer[i + 1]
            if (self.mode == "single"):
                size = (Ip.shape[1], Ip.shape[0])
                Iexpand = cv2.pyrUp(Iexpand,dstsize=size)
            CenterSrnd = np.abs(Ip - Iexpand)
            CenterSrndPyramid.append(CenterSrnd**self.p)
        self.SorfPyramid[self.numberOfLevel-1] = CenterSrndPyramid[
            self.numberOfLevel-1]
        for i in range(self.numberOfLevel-2, -1,-1):
            if (self.mode == "single"):
                size = (CenterSrndPyramid[i].shape[1],
                        CenterSrndPyramid[i].shape[0])
                mulUp = cv2.pyrUp(self.SorfPyramid[i + 1], dstsize=size)
            else:
                mulUp = self.SorfPyramid[i + 1]
            self.SorfPyramid[i] = self.Wn[i] * CenterSrndPyramid[i]**self.mu[i] + (1 -
                                                                     self.Wn[i])*(mulUp)
            self.SorfPyramid[i] = np.array(self.SorfPyramid[i])
        for i in range(0, self.numberOfLevel):
            self.SorfPyramid[i] = self.NormAndConvertImagetoDouble(self.SorfPyramid[i])
            if self.debug:
                val = ax[j][i % 4].imshow(self.SorfPyramid[i],cmap="gist_heat")
                ax[j][i % 4].set_title("layer: {}".format(i))
            if i % 4 == 3:
                j += 1
            i += 1
        if self.debug:
            fig.canvas.set_window_title("GenerateSorfPyramid")
            fig.suptitle("SORF Pyramid with MU {}".format(self.mu))
            fig.show()
            plt.colorbar(val, ax=ax.ravel().tolist())

    def modulationExponent(self, delta,i):
        return delta*(np.ones(np.array(self.SorfPyramid[i]).shape))*np.amax(self.SorfPyramid[
                                                                       i]) - self.SorfPyramid[i]
    def buildExp(self):
        fig, ax = plt.subplots(3, 3)
        j = 0
        for i in range(0,self.numberOfLevel):
            self.deltaSmall.append(self.modulationExponent(1,i))
            ax[j][i % 3].imshow(self.deltaSmall[i])
            if i % 3 == 2:
                j += 1
            i += 1
        if self.debug:
            fig.canvas.set_window_title("buildExp")
            fig.show()

    def nrrImageShow(self):
        if self.debug:
            fig, ax = plt.subplots(2, 4)
        j =0
        for i in range(0,self.numberOfLevel):
            C = np.arange(self.ContrastPyramid[i].min(),self.ContrastPyramid[i].max(),0.01)
            S = self.SorfPyramid[i]
            SNorm = self.NormAndConvertImagetoDouble(S);
            SLin = (1 - SNorm)
            lmb = self.lmb[i]
            Gamma = lmb * SLin;
            C[C == 0] = 1e-6
            MaxGamma = Gamma.max()
            Gamma = Gamma.max() * np.ones(np.size(C))
            Gamma[(Gamma < self.GammaMin)] = self.GammaMin
            R = self.Rmax[i]/(self.alpha + ((self.beta/ C)**(Gamma))) + self.b[i]
            if self.debug:
                val = ax[j][i % 4].plot(C, R, 'r')
            while(R.min()>0 or R.max()<1):
                self.Rmax[i] = (1 - self.b[i]) * (self.alpha + ((self.beta / self.ContrastPyramid[i].max()) ** (MaxGamma)))
                R = self.Rmax[i] / (self.alpha + ((self.beta / C) ** (Gamma))) + self.b[i];
                if(R.min()>0):
                    self.b[i]-=0.005
                    R = self.Rmax[i] / (self.alpha + ((self.beta / C) ** (Gamma))) + self.b[i]
                else:
                    break
            if self.debug:
                val = ax[j][i % 4].plot(C, R, 'b')
                ax[j][i % 4].set_title(
                    "NKR Rmax{:.2f},b{:.2f}, MaxGamma{:.2f}".format(self.Rmax[i], self.b[i],MaxGamma))
                ax[j][i % 4].set_xlabel('Cn')
                ax[j][i % 4].set_ylabel('Respond')
            if i % 4 == 3:
                j += 1
            i += 1
        if self.debug:
            if self.debug:
                fig.canvas.set_window_title("Naka Rushton Response")
                fig.suptitle("Naka Rushton Response")
                fig.legend(('Original', 'Approximate'))
                fig.show()
        return

    #Naka Rushton Response
    def nrr(self):
        Rn = 0
        j=0
        value = []
        Cinv = []
        if self.debug:
            fig, ax = plt.subplots(2, 4)
            fig1, bx = plt.subplots(2, 4)

        for i in range(0,self.numberOfLevel):
            C = self.ContrastPyramid[i]
            S = self.SorfPyramid[i]
            lmb = self.lmb[i]
            if i < self.numberOfLevel-1:
                lmb = np.average(C)
            SNorm = self.NormAndConvertImagetoDouble(S);
            SLin = (1 - SNorm)
            Gamma = lmb * SLin;
            C[C == 0] = 1e-6
            Gamma[(Gamma < self.GammaMin)] = self.GammaMin
            self.GammaPyrmaid.append(Gamma)
            R = self.Rmax[i]/(self.alpha + ((self.beta/ C)**(Gamma))) + self.b[i]
            value.append(R)
            Cinv.append(self.beta/((self.Rmax[i]/(value[i]-self.b[i])-self.alpha)))
            idx = ((C > self.lowerLimit) & (C < self.upperLimit))
            Cinv[i][~idx] = self.ContrastPyramid[i][~idx]
            maxContrast = 5 * C
            Cinv[i][Cinv[i] > maxContrast] = maxContrast[Cinv[i] > maxContrast];
            if self.debug:
                val1 = ax[j][i % 4].imshow(value[i])
                val = bx[j][i % 4].imshow(Cinv[i])
            self.ResponsePyramid.append(Cinv[i])
            if i % 4 == 3:
                j += 1
            i += 1
        if self.debug:
            if self.debug:
                fig.canvas.set_window_title("Naka Rushton Response")
                fig.suptitle("Naka Rushton Response")
                fig.show()
                fig1.canvas.set_window_title("Inverse contrast")
                fig1.suptitle("Inverse contrast")
                fig1.show()
                plt.colorbar(val, ax=ax.ravel().tolist())
                plt.colorbar(val1, ax=bx.ravel().tolist())

    def pyramidCollapse(self):
        Bn = self.GDBContainer
        if self.debug:
            fig, ax = plt.subplots(2, 4)
        j=0
        i=0
        for n in range(self.numberOfLevel-2,-1,-1):
            if (self.mode == "single"):
                size = (self.ResponsePyramid[n].shape[1],
                        self.ResponsePyramid[n].shape[0])
                next = cv2.pyrUp(Bn[n+1],dstsize=size)
            else:
                next = Bn[n+1]
            Bn[n] = self.ResponsePyramid[n]*next
            if self.debug:
                ax[j][i%4].imshow(Bn[n])
                ax[j][i % 4].set_title("layer: {}".format(self.numberOfLevel-2-i))
            if i % 4 == 3:
                j += 1
            i+=1
        NewImage = Bn[0].astype(float) - np.amin(Bn[0].astype(float));
        NewImage = NewImage / np.amax(NewImage)
        self.result = NewImage
        if self.debug:
            fig.show()
            fig.canvas.set_window_title("ACC result")
            fig.suptitle("ACC result")
            ax[-1, -1].axis('off')

            plt.figure()
            plt.imshow(NewImage)
            plt.show(block=False)

def runHdrAlgoritham(file,debug,mask=None):
    #todo: debug change the name of the windows to meaningful name.
    hdrRuner = imageReader(file,None,debug = False)
    hdrRuner.convertRawToImage()
    hdrFunction = hdr(hdrRuner.path, hdrRuner.imageDnoise,debug,mode="single")
    hdrFunction.GDB()
    hdrFunction.ContrastPyr()
    hdrFunction.GenerateSorfPyramid()
    hdrFunction.nrrImageShow()
    hdrFunction.nrr()
    hdrFunction.pyramidCollapse()
    return hdrFunction,hdrRuner

def main():
    files = []
    path = "./data/"
    for r, d, f in os.walk(path):
        for file in f:
            files.append(os.path.join(r, file))

    for file in files:
        ds = pydicom.dcmread(file)
        runHdrAlgoritham(file,False)

if __name__ == '__main__':
    main()
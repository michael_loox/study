import LF_01_06
import cv2
import numpy as np

def LF(img, imageMask, tetaRes=1, imgRes=8, plotFlag=0,logLevel=0,maxType=LF_01_06.max8bit,debug=False):

    cv2.destroyAllWindows()
    was_imgRes_char = 0
    stringType = "uint8"
    if maxType == LF_01_06.max16bit:
        stringType="uint16"

    #if (img.shape[1 - 1] != 1024 or img.shape[2 - 1] != 1024):
    #    print('Resizing image to 1024X1024')
    #    I = cv2.resize(img, np.array([480, 480]), interpolation=cv2.INTER_LANCZOS4)

    # Constants per scale
    percentile_per_res = np.ones(imgRes)
    percentile_per_res = percentile_per_res * 95

    percentile_per_res[0:1] = 97

    percentile_per_res[2:3] = 95

    percentile_per_res[4:8] = 80

    m1 = 1
    m2 = 2

    # Res weight factor for sum
    tau = [14,12,10,8,6,4,2,1]
    tau = [1,2,4,8,16,32,64,128]
    #tau = [128,64,32,16,8,4,2,1]
    # Fac length per resulotion
    #Fac_array = [10,9,8,7,7,7,5,5]
    Fac_array = [10,9,8,7,7,7,5,5,5,5,4,4,4,3,3,3]
    # SE element radius per res

    #se_array = [0,2,3,4,5,5,5,6]
    se_array = [0,2,3,4,5,5,5,6,6,6,7,7,7,8,8,8]
    #se_array = np.ones(imgRes)*1
    gamma = imgRes


    # This is an array of images, containing the image for each resultion
    pyrmid_res = []

    im_res_array =  np.zeros([img.shape[0], img.shape[1], imgRes])
    im_res_mask_array =  np.zeros([img.shape[0], img.shape[1], imgRes])

    # The images of each scale will be summed into this image.
    T =  np.zeros([img.shape[0], img.shape[1]])

    # calculate a cache value
    LnormCache, LCache = LF_01_06.gabor1(tetaRes)
    return
    i = range(0,tetaRes)
    tatCache = np.pi * np.array(i).astype(float) / tetaRes

    #Iterate for each image scale
    start = time.time()
    for j in range(imgRes,0,-1):

        img = np.array(img)
        # The resize factors    for the row and column dimension of the image.
        RSfactorR = round(img.shape[0]/np.max([1,j]))
        RSfactorC = round(img.shape[1]/np.max([1,j]))
        # Instead of changing the kernel size, we   applied constant sized kernels on Gaussian   pyramid array of the input image
        img = Image.fromarray(img)
        imgS = img.resize(np.array([RSfactorR, RSfactorC]),resample=Image.LANCZOS)

        # This is an array of images, containing the image
        #for each orientation
        im_orientation_array =  np.zeros([np.size(imgS, 0), np.size(imgS, 1), tetaRes])
        # Iterate for each filter orientation
        cfrResult = 0
        if (debug):
            cfrResult = []
        for i in range(0,tetaRes):


            #Angles Calculations
            tat = tatCache[i]

            Lnorm = LnormCache[:,:,i]

            # L is the gabor kernel
            L = LCache[:,:,i]

            Lnorm = convolve2d (Lnorm, L, mode='same')

            # The normalization factor of the gabor kernel(1 / g)
            CONVnorm = Lnorm[int((L.shape[0] / 2)), int((L.shape[1] / 2))]

            # # Conv of the input image with the gabor oriented kernel
            # cRF represents the classical receptive field response, to which the LF will be added.
            # cRF = conv2(imgS, L, 'same') / CONVnorm
            cRF = cv2.filter2D(np.array(imgS), -1, np.array(L) ) / CONVnorm
            cRF = normalize_image(cRF)
            if debug:
                cfrResult.append(cRF)

            # img0(Eq. 1) contains both positive and negative values.
            # We will separate the image to two images, one with the positive values ( and zero instead negative values)
            # and the other with the negative values.
            # These two images will be considered differently with the following calculations,
            # as if they were obtained from two different kernels.
            cRF_p = np.maximum(cRF, 0)
            cRF_n = np.maximum(-cRF, 0)

            # All crf below 5
            # considered as noise. Zero it.
            THR = np.abs(cRF).max()
            cRF_p = ((cRF_p > 0.05).astype(int) * THR) * cRF_p
            cRF_n = ((cRF_n > 0.05).astype(int) * THR) * cRF_n


            # Michael Continue
            #Fac = max(5, 10 / j)
            Fac = Fac_array[j-1]

            #LF calc for the positive and negative images
            #LF
            [LF_p, cRF_p_nr] = LFsc(cRF_p, tat, Fac)
            [LF_n, cRF_n_nr] = LFsc(cRF_n, tat, Fac)
            LF_P_THR = np.abs(LF_p).max()
            LF_p = ((LF_p > 0.05).astype(int) * LF_P_THR) * LF_p
            LF_N_THR = np.abs(LF_n).max()
            LF_n = ((LF_n > 0.05).astype(int) * LF_N_THR) * LF_n

            LF_p = normalize_image(LF_p)
            LF_n = normalize_image(LF_n)

            #cRF_p_nr = normalize_image(cRF_p_nr)
            #cRF_n_nr = normalize_image(cRF_n_nr)

            # This is the details enhancement step(eq9 from hava dr.proposal)
            # max⁡(0, LFsignal - (const1 * NRsignal)) * const2
            # For high values of const1, the outcome would contain mostly facilitation from neighboring pixels.
            # For low values, the facilitation will be induced not only from the surrounding  but also from the signal  itself.
            # The aim of const2 component is to avoid invention of information and completion at areas without sub threshold
            # information
            sub_threshold_p = np.percentile(nonzeroPythonMatlab(cRF_p + LF_p), percentile_per_res[j-1], interpolation='midpoint')
            sub_threshold_n = np.percentile(nonzeroPythonMatlab(cRF_n + LF_n), percentile_per_res[j-1], interpolation='midpoint')
            alpha_p = 0
            alpha_n = 0

 #           LF_p = np.maximum(0, LF_p - alpha_p * cRF_p_nr) * sigmoid((cRF_p + LF_p) - sub_threshold_p)
  #          LF_n = np.maximum(0, LF_n - alpha_n * cRF_n_nr) * sigmoid((cRF_n + LF_n) - sub_threshold_n)

            if(debug):
                val = plt.imshow(np.maximum(0, LF_p), cmap='jet')
                plt.suptitle("func".format(j))
                plt.colorbar(val)
                plt.waitforbuttonpress()
                plt.close()

            LF_p = np.maximum(0, LF_p) * sigmoid((cRF_p + LF_p) - sub_threshold_p)
            LF_n = np.maximum(0, LF_n) * sigmoid((cRF_n + LF_n) - sub_threshold_n)

            LF_p = normalize_image(LF_p)
            LF_n = normalize_image(LF_n)

            im_orientation_array[:,:, i] = -(0.5 * LF_n) + (0.5 * LF_p)
            im_orientation_array[:,:, i]= (np.abs(im_orientation_array[:,:, i])** m1)*np.sign(im_orientation_array[:,:, i])

            #end
        # Summing of the orientation responses per resolution Take only the repsonse from the leading orientation.

        # T_j = max(im_orientation_array, [], 3)
        if(debug):
            fromContainerToPlot(cfrResult)


        if (debug):
            showAarray(im_orientation_array,"",tetaRes)
            plt.waitforbuttonpress()

        # orientation max

        T_j = maxk(im_orientation_array, 1, 3)

        if (debug):
            val = plt.imshow(T_j,cmap='jet')
            plt.suptitle("Max Of Orientations Level {}".format(j))
            plt.colorbar(val)
            plt.waitforbuttonpress()
            plt.close()


        if (T_j.shape.__len__()>2):
            T_j = np.median(T_j, 3)

        T_j = (np.abs(T_j) ** 1 / m1) * np.sign(T_j)

        # Resizing back to the original image size.
        T_j = Image.fromarray(T_j)
        pyrmid_res.append(T_j)
        im_res_array[:, :, j-1] = np.array(T_j.resize(np.array([int(img.size[0]), int(img.size[1])]), resample=Image.LANCZOS))
        # Summing the results of each image scale
        val = im_res_array[:, :, j-1]
        val = val - val.min()
        tensorThresh = (val * maxType).astype(stringType)
        ret, mask = cv2.threshold(tensorThresh,0,1,cv2.THRESH_OTSU)
        if logLevel>1:
            cv2.imshow("mask", mask*maxType)
            cv2.waitKey(10)

        ## Morphological Edits to mask
        se_rad = se_array[j-1]
        se = morphology.disk(se_rad)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, se)
        if logLevel>1:
            print(j)
            print(se_rad)
            cv2.imshow("MORPH_CLOSE",mask*max16bit)
            cv2.waitKey(5)
        # Remove unnecessary borders.
        t_mask = mask
        r_1 = 1
        r_2 = 1
        r_3 = 1
        r_4 = 1

        for b in range(1,51):

            if mask[b,:].min()!=0:
                t_mask[b,:] = 0
                r_1 = r_1 + 1

            if mask[b,:].min()!=0:
                t_mask[:, b] = 0
                r_2 = r_2 + 1

            if mask[mask.shape[0] - b,:].min()!=0:
                t_mask[mask.shape[0]- b,:] = 0
                r_3 = r_3 + 1

            if mask[:,mask.shape[1]-b].min()!=0:
                t_mask[:,mask.shape[1]-b] = 0
                r_4 = r_4 + 1

            mask = t_mask

            border = 5
            if r_1 > 1:
                mask[r_1: r_1 + border,:] = 0

            if r_2 > 1:
                    mask[:, r_2: border + r_2] = 0

            if r_3 > 1:
                mask[mask.shape[0] - r_3 - border: mask.shape[0],:] = 0

            if r_4 > 1:
                mask[:,  mask.shape[1] - r_4 - border:  mask.shape[1]] = 0

        # Connected Components
        #all_labels = measure.label(mask,background=0,connectivity=2)
        num_labels, all_labels = cv2.connectedComponents(mask,
                                                     connectivity=8)
        temp_mask = mask
        if logLevel>1:
            cv2.imshow("layer",temp_mask*maxType)
            cv2.waitKey(10)
        temp_mask = saveMaxNumberOfConnect(all_labels,"int32")
        mask = temp_mask.astype(stringType)
        if logLevel>1:
            cv2.imshow("new layer",mask*maxType)
            cv2.waitKey(10)
        im_res_mask_array[:,:, j-1] = mask
        #im_res_array[:,:, j-1] = im_res_array[:,:, j-1] / 2 ** (sigmoid(abs(j - 2)))
        # Weight each resolution differently
        T = T + im_res_array[:,:, j-1]

        if(debug):
            plt.imshow(T)
            plt.waitforbuttonpress()
            plt.close()
    collapsImage = collaps(pyrmid_res)

    if (debug):
        plt.imshow(normalize_image(collapsImage))
        plt.waitforbuttonpress()
        plt.close()
    end = time.time()
    print(end - start)

    im_res_arrayOut = im_res_array
    im_res_mask_arrayOut = im_res_mask_array

    maskOut = np.zeros(mask.shape);
    # moving avrage
    # for i in range (0,imgRes - 1,1):
    #     val = np.max(im_res_mask_array[:,:, np.array(range(imgRes-i-1, imgRes-i-2, -1))],2)*(tau[i]/np.sum(tau))
    #     a = val * imageMask
    #     maskOut = maskOut + a

    maskOut = np.median(im_res_mask_array,2)
    idxOut = abs(maskOut) > 0


    maskOut = nthroot(maskOut, 3)

    maskOut = maskOut * idxOut

    #T = T * (maskOut)

    # Whole image out treatment # formula 24
    T_tag = (1 / gamma) * (np.abs(T) ** 1 / m2) * np.sign(T)

    imgOut = Image_normalize_image(T_tag)
    maskOut = Image_normalize_image(maskOut)
    maskOut = maskOut
    return imgOut, maskOut, im_res_arrayOut, im_res_mask_arrayOut

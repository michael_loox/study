import os.path

import cv2
import numpy as np
import matplotlib.pyplot as plt
from utill import runOverImage,normalize_image,sortImageList
from cv2 import imshow
from LF_01_06 import LF
import re
eps = 2.2204e-16

max16bit = 2**16-1
max8bit = 2**8-1

def LF_main(img,maskValue,fileName=None, plotFlag=True, logLevel=0,maxType=max8bit):

    if (img.shape[1 - 1] != 1024 or img.shape[2 - 1] != 1024):
        print('Resizing image to 1024X1024')
        I = cv2.resize(img, np.array([480, 480]), interpolation=cv2.INTER_LANCZOS4)
    else:
        I = img

    I = img
    tetaRes = 32
    imgRes = 8

    I = I.astype('float')
    I = normalize_image(I)

    T_tag, maskOut, im_res_arrayOut, im_res_mask_arrayOut = LF(I, tetaRes, imgRes,logLevel=logLevel,maxType=maxType)
    if fileName!=None:
        file = os.path.basename(fileName)
        cv2.imwrite(os.getcwd()+"\\vainMask\\"+file,maskOut * maxType)
        cv2.imwrite(os.getcwd()+"./Tag/"+file, T_tag * maxType)
    if plotFlag:
        fig, ax = plt.subplots(2, 2)
        ax[0][0].set_title('Original image')
        cmap = plt.cm.get_cmap("jet")
        ax[0][0].imshow(I, cmap=cmap)
        ax[0][1].set_title(str("Teta Resolutions:" + str(tetaRes) + '\nImage Size Resolutions:' + str(imgRes)))
        ax[0][1].imshow((maskOut * maxType).astype("uint8"), cmap=cmap)
        seg = maxType - (maskOut * maxType) > 0
        ax[1][0].set_title("maskOut Parent")
        ax[1][0].imshow(np.array(seg),cmap='gray', vmin=0, vmax=1)
        ax[1][1].set_title(str("Vains view"))
        ax[1][1].imshow(T_tag-T_tag.min(), cmap='gray')

        fig.show()
        plt.waitforbuttonpress()
        plt.close('all')

    #todo: We can do better with nr
    I_E = (I)*(T_tag-T_tag.min())+I
    I_E = normalize_image(I_E*maskValue)
    if plotFlag:
        fig, ax = plt.subplots(1, 2)
        ax[0].set_title("origin image")
        ax[0].imshow(I, cmap='gray')
        ax[1].set_title("Vein enhance image")
        ax[1].imshow(I_E, cmap='gray')
        fig.show()
        plt.waitforbuttonpress()
        plt.close('all')

    imgOut = I_E
    return imgOut,T_tag,maskOut

def vainExtractor(image,fileName,mask=None,logLevel=0,maxType=max8bit):
    cv2.destroyAllWindows()
    if logLevel>1:
        cv2.imshow("new",image)
        cv2.waitKey(10)
    if mask is None:
        mask = np.ones(image.shape)
    fileName = str(fileName).replace(".txt",".jpg")
    LF_main(image,mask,fileName,logLevel=logLevel,maxType=maxType)


def main():
    path1 = "C:/study/study/final/hel/"
    files = runOverImage(path1)
    str_match = [s for s in files if "1side" in s]
    str_match = sortImageList(str_match)
    maxTypeValue = max8bit
    for file in str_match:
        name, extension = os.path.splitext(file)
        if extension==".jpg":
            image = cv2.imread(file,cv2.IMREAD_GRAYSCALE)
        else:
            image = np.loadtxt(file)
            image = cv2.normalize(image, None, 0, max8bit,
                                    cv2.NORM_MINMAX).astype("uint8")
            maxTypeValue = max8bit
        vainExtractor(image,file,debug=False,maxType=maxTypeValue)
    return 1


if __name__ == '__main__':
    main()
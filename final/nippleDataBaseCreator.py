import os
import cv2
import numpy as np
from nipple import contrastMap,nippleDetection
from utill import getNormilized,runOverImage
import sys
sys.path.insert(1, 'C:\study\study')
import hrdLabs as hdr
from extractor import extractor
import random as rand

xm = 0
ym = 0
mouse = 0
image = None

def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping
    # if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True
	# check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False
        # draw a rectangle around the region of interest
        cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
        cv2.imshow("image", image)

def zeroingMask(mask):
    mask=0

def imageCropMouse(side,mask,fileName):
    global image,refPt
    clone = image.copy()
    cloneMask = mask.copy()
    refPt = [0]
    refPtList = []
    print(fileName)
    while True:
        # display the image and wait for a keypress
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF
        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            image = clone.copy()
        # if the 'c' key is pressed, break from the loop
        elif key == ord("e"):
            break
        elif key == ord(" "):
            refPtList.append(refPt)
            break
        elif key == ord("c"):
            image = clone.copy()
            refPt = []
        elif key == ord("n"):
            refPtList.append(refPt)
            if len(refPtList)>=2:
                break

    if len(refPt) == 2:
        counter = 0
        for point in refPtList:
            refPt = point
            sizeX=int((refPt[1][1]-refPt[0][1])/2)
            sizeY = int((refPt[1][0]-refPt[0][0])/2)
            roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
            cv2.imwrite("./cnn/nipple/"+side+fileName+str(counter)+".jpg",roi*256)
            np.savetxt("./cnn/nippleM/" + side + fileName +str(counter)+ ".jpg", roi)
            mask[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]] = 0
            for index in range(0,10):
                a = rand.choice(np.argwhere(np.array(mask) > 0))
                roi = clone[a[0]-sizeX:a[0]+sizeX, a[1]-sizeY:a[1]+sizeY]
                if(roi.shape[0]==0 or roi.shape[1]==0):
                    continue
                cv2.imwrite("./cnn/notNipple/" + side +str(index)+ fileName +str(counter)+ ".jpg", roi*256)
                np.savetxt("./cnn/notNippleM/" + side + fileName +str(counter)+ ".jpg", roi)
                const = 5
                const1 = 5
                if a[0]<5:
                    const = a[0]
                if a[1]<5:
                    const1 = a[1]
                print(a[0],a[1])
                mask[a[0]-const:a[0]+5, a[1]-const1:a[1]+5] = 0
            counter+=1

cv2.namedWindow("image")
cv2.setMouseCallback("image", click_and_crop)

def main():
    global xm,ym,image
    mouse = None
    files = []
    path = "../data/sick"
    path = "../data/healthy"
    path = "../data"
    saveNipplePath = "./nippleFirstSign"

    mode = 'hdr'
    files = runOverImage(path)
    while len(files)!=0:
        index = np.ceil(rand.random()*len(files)).astype("uint32")
        file = files[index]
        files.remove(files[index])
        val, hdrRuner = hdr.runHdrAlgoritham(file,False)
        rgb_out = getNormilized(val.result)
        ext = extractor(hdrRuner.imageFileRaw, rgb_out, file, debug=False)
        body, mask, resultL, labelsL, resultR, labelsR, imageR, imageL = ext.connected_component_label(file)
        image = rgb_out*mask
        imageCropMouse("L",rgb_out,os.path.basename(file))

if __name__ == "__main__":
    main()
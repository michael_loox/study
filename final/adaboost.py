import utill
import numpy as np
import cv2
from skimage import transform

import sys
from time import time

import numpy as np
import matplotlib.pyplot as plt

from dask import delayed

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score

from skimage.data import lfw_subset
from skimage.transform import integral_image
from skimage.feature import haar_like_feature
from skimage.feature import haar_like_feature_coord
from skimage.feature import draw_haar_like_feature

def extract_feature_image(img, feature_type, feature_coord=None):
    """Extract the haar feature for the current image"""
    ii = integral_image(img)
    return haar_like_feature(ii, 0, 0, ii.shape[0], ii.shape[1],
                             feature_type=feature_type,
                             feature_coord=feature_coord)

def main():
    global xm,ym,image
    mouse = None
    files = []
    pathNipple = "cnn/nippleM"
    pathNNipple = "cnn/notNippleM"

    mode = 'hdr'
    filesNipple  = utill.runOverImage(pathNipple)
    filesNippleN = utill.runOverImage(pathNNipple)
    # for item in filesNipple:
    #     data = np.array(np.loadtxt(item))
    #     I = cv2.resize(data, np.array([16,16]), interpolation=cv2.INTER_LANCZOS4)
    #     integral_image = transform.integral.integral_image(I)
    images = lfw_subset()
    # To speed up the example, extract the two types of features only
    feature_types = ['type-2-x', 'type-2-y']

    # Build a computation graph using Dask. This allows the use of multiple
    # CPU cores later during the actual computation
    X = delayed(extract_feature_image(img, feature_types) for img in images)
    # Compute the result
    t_start = time()
    X = np.array(X.compute(scheduler='single-threaded'))
    time_full_feature_comp = time() - t_start

    # Label images (100 faces and 100 non-faces)
    y = np.array([1] * 100 + [0] * 100)

    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=150,
                                                        random_state=0,
                                                        stratify=y)

    # Extract all possible features
    feature_coord, feature_type = \
        haar_like_feature_coord(width=images.shape[2], height=images.shape[1],
                                feature_type=feature_types)


if __name__ == '__main__':
    main()
import matplotlib.pyplot as plt
from extractor import extractor
from nipple import nippleDetection
from utill import cv2, np, runOverImage, getNormilized, normalize_image, saveResult
import os
from IPython import get_ipython

path1 = "./ImageTest/"
saveLocation = "./Stage3Loction"


def main():
    files = runOverImage(path1,"")
    stage2 = False
    for file in files:
        with open(file, 'rb') as f:
            # load the image
            print("working on: {}".format(file))
            image = np.load(f)
            # Run on the breast
            ext = extractor(image, image, "\\", debug=False)
            body, mask, resultR, labelsR, resultL, labelsL, imageR, imageL = ext.connected_component_label2(file)
            name = str(file).split("\\")
            pathDir = os.path.join(os.getcwd(), saveLocation, name[1], name[2]).replace(".npy", "")
            inx = name.index("npy")
            pathDirR = os.path.join(pathDir,name[inx-1],"hdr",name[inx+2]+"resultL.jpg")
            saveResult(resultL, pathDirR)
            pathDirR = os.path.join(pathDir,name[inx-1],"hdr",name[inx+2]+"resultR.jpg")
            saveResult(resultR, pathDirR)
            if stage2:
                # Find the nipple.
                imgC_C = cv2.normalize(imageR, None, 0, 255,
                                       cv2.NORM_MINMAX).astype(np.uint8())
                cropR, NippleR = nippleDetection(resultR, imgC_C, labelsR, 0, debug=False)
                pathDirR = os.path.join(pathDir, name[inx - 1], "crop", name[inx + 2] + "resultL.jpg")
                saveResult(NippleR, pathDirR)
                imgC_C = cv2.normalize(imageL, None, 0, 255,
                                       cv2.NORM_MINMAX).astype(np.uint8())
                cropL, NippleL  = nippleDetection(resultL, imgC_C, labelsL, 0, debug=False)
                pathDirR = os.path.join(pathDir, name[inx - 1], "crop", name[inx + 2] + "resultR.jpg")
                saveResult(NippleL, pathDirR)

    return 1


if __name__ == '__main__':
    main()

import sys; print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['C:\\study\\study', 'C:/study/study'])
import random as rand
from utill import cv2,np, runOverImage,getNormilized,normalize_image
from nipple import contrastMap,nippleDetection,nippleUsingKnn
import hrdLabs as hdr
from extractor import extractor
import SorfProcessing as SP
import attentionModel as at
import matplotlib.pyplot as plt
from skimage import exposure
import os

def main():
    files = []
    path2 = "../data/new/"
    path1 = "../data/"
    #path1 = "../data/healthy/900"
    #path1 = "../data/healthy/42"
    #path1 = "../data/sick/263"
    saveNipplePath = "../nippleFirstSign"
    logLevel = 0;
    mode = 'hdr'
    files = runOverImage(path1)
    for file in files:
        val, hdrRuner = hdr.runHdrAlgoritham(file,False)
        rgb_original = getNormilized(hdrRuner.imageValue)
        rgb_hdr = getNormilized(val.result)
        imageVal = (rgb_original*(2**16-1)).astype("uint16")
        img_eq = exposure.equalize_hist(rgb_original)

        # Adaptive Equalization
        img_adapteq = exposure.equalize_adapthist(imageVal, clip_limit=0.001,nbins=1500)

        ext = extractor(hdrRuner.imageFileRaw, rgb_hdr, file,debug=True)
        body, mask, resultL, labelsL, resultR, labelsR, imageR, imageL = ext.connected_component_label(file)
        phase1 = True
        if phase1:
            plt.imshow(rgb_hdr * mask)
            plt.waitforbuttonpress()
            fig, ax = plt.subplots(2, 2)
            ax[0][0].set_title("origin image")
            ax[0][0].imshow(rgb_original, cmap='gray')
            ax[1][0].set_title("equalize hist")
            ax[1][0].imshow(img_eq, cmap='gray')
            ax[1][1].set_title("adapt hist")
            ax[1][1].imshow(img_adapteq, cmap='gray')
            ax[0][1].set_title("ACC algorithm")
            ax[0][1].imshow(rgb_hdr*mask, cmap='gray')
            np.savetxt('fullHDR.txt', (rgb_hdr*mask), delimiter=",")
            fig.show()
            plt.waitforbuttonpress()
            plt.close('all')

        if phase1:
            fullPath = os.getcwd()
            name = str(file).split("\\")
            value = str(name[0]).split("/")
            pathDir = os.path.join(fullPath,".\\dicomFullImage\\",value[2], name[1])
            pathOriginal = os.path.join(pathDir ,name[-1].replace(".txt","O.txt"))
            pathHDR = os.path.join(pathDir, name[-1].replace(".txt", "HO1.txt"))
            os.makedirs(pathDir,exist_ok=True)
            print("saving to:\n{}".format(pathOriginal))
            np.savetxt(pathOriginal, (rgb_original), delimiter=",")
            np.savetxt(pathHDR, (rgb_hdr*mask), delimiter=",")
            plt.title("Input Original Image")
            plt.imshow(rgb_original)
            plt.savefig(pathOriginal+".jpg")
            plt.title("After ACC \"HDR\" Algorithm  Image")
            plt.imshow((rgb_hdr*mask))
            plt.savefig(pathHDR+".jpg")


        phase2 = False
        if phase2:
            rgb_out = normalize_image(imageR)
            imgC = contrastMap(rgb_out, False)
            imgC_C = cv2.normalize(imageR, None, 0, 255,
                                      cv2.NORM_MINMAX).astype(np.uint8())
            cropR, RigthCup = nippleDetection(resultR, imgC_C, labelsR, 0, debug=False)
            rgb_out = normalize_image(imageL)
            imgC = contrastMap(rgb_out, False)
            imgC_C = cv2.normalize(imageL, None, 0, 255,
                                      cv2.NORM_MINMAX).astype(np.uint8())
            cropL,leftCup = nippleDetection(resultL, imgC_C, labelsL,1,debug=False)


        if phase2:
            fullPath = os.getcwd()
            name = str(file).split("\\")
            value = str(name[0]).split("/")
            pathDir = os.path.join(fullPath, ".\\nipple\\")
            RigthCupPathB = os.path.join(pathDir, name[-1].replace(".txt", "RigthCupB.jpg"))
            leftCupPathB = os.path.join(pathDir, name[-1].replace(".txt", "leftCupB.jpg"))
            cv2.imwrite(RigthCupPathB,RigthCup)
            cv2.imwrite(leftCupPathB, leftCup)

        phase3 = True


        if phase3:
            imgOutL = at.vainExtractor(resultL, file, labelsL, logLevel=logLevel)
            imgOutR = at.vainExtractor(resultR, file, labelsR, logLevel=logLevel)

        if phase3:
            fullPath = os.getcwd()
            name = str(file).split("\\")
            value = str(name[0]).split("/")
            pathDir = os.path.join(fullPath, ".\\dicomFullImage\\", value[2], name[1])
            RigthCupPathB = os.path.join(pathDir, name[-1].replace(".txt", "RigthCupB.txt"))
            leftCupPathB = os.path.join(pathDir, name[-1].replace(".txt", "leftCupB.txt"))
            RigthCupPathHDR = os.path.join(pathDir, name[-1].replace(".txt", "RigthCupHDR.txt"))
            leftCupPathHDR = os.path.join(pathDir, name[-1].replace(".txt", "leftCupHDR.txt"))
            RigthCupPath = os.path.join(pathDir, name[-1].replace(".txt", "RigthCupF.txt"))
            leftCupPath = os.path.join(pathDir, name[-1].replace(".txt", "leftCupF.txt"))
            os.makedirs(pathDir, exist_ok=True)
            print("saving to:{}".format(RigthCupPath))
            np.savetxt(RigthCupPathB, imageR, delimiter=",")
            np.savetxt(leftCupPathB, imageL, delimiter=",")
            np.savetxt(RigthCupPathHDR, resultR, delimiter=",")
            np.savetxt(leftCupPathHDR, resultL, delimiter=",")
            np.savetxt(RigthCupPath, np.array(imgOutR[0]), delimiter=",")
            np.savetxt(leftCupPath, np.array(imgOutL[0]), delimiter=",")

            plt.imshow(imageR)
            plt.savefig(RigthCupPathB+".jpg")
            plt.imshow(imageL)
            plt.savefig(leftCupPathB+".jpg")

            plt.imshow(resultR)
            plt.savefig(RigthCupPathHDR+".jpg")
            plt.imshow(resultL)
            plt.savefig(leftCupPathHDR+".jpg")

            plt.imshow(np.array(imgOutR[0]))
            plt.savefig(RigthCupPath+".jpg")
            plt.imshow(np.array(imgOutL[0]))
            plt.savefig(leftCupPath+".jpg")


def mainDataBasePrint():
    path1 = "cnn\\notNipple"
    files = runOverImage(path1)
    shape = 10
    fig, ax = plt.subplots(shape, shape,gridspec_kw={'wspace':0, 'hspace':0},squeeze=True)
    counter = 0
    for x in range(0,shape):
        y=0
        while y<shape:
            print(len(files))
            index = np.floor(rand.random()*len(files)).astype("uint32")
            file = files[index]
            files.remove(file)
            image = cv2.imread(file)
            cv2.imshow("new",image)
            if(image is None or np.average(image)<70):
                continue
            image = cv2.resize(image, [32,32])
            counter += 1
            ax[x][y].axis("off")
            ax[x][y].imshow(image, aspect='auto')
            y = y + 1
    fig.show()
    plt.waitforbuttonpress()


if __name__ == "__main__":
    #mainDataBasePrint()
    main()
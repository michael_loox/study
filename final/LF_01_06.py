import time

import cv2
import numpy as np
from scipy.signal import convolve2d
from utill import normalize_image,fspecial,Image_normalize_image,filterCretor
import utill as ut
import scipy as sc
from skimage import morphology
from skimage.morphology import skeletonize
from skimage import measure
import matplotlib.pyplot as plt
from PIL import Image

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import math

eps = 2.2204e-16

max16bit = 2**16-1
max8bit = 2**8-1

DEBUG = False

def oldMaskWorker(mask):
    t_mask = mask
    r_1 = 1
    r_2 = 1
    r_3 = 1
    r_4 = 1

    for b in range(1, 51):

        if mask[b, :].min() != 0:
            t_mask[b, :] = 0
            r_1 = r_1 + 1

        if mask[b, :].min() != 0:
            t_mask[:, b] = 0
            r_2 = r_2 + 1

        if mask[mask.shape[0] - b, :].min() != 0:
            t_mask[mask.shape[0] - b, :] = 0
            r_3 = r_3 + 1

        if mask[:, mask.shape[1] - b].min() != 0:
            t_mask[:, mask.shape[1] - b] = 0
            r_4 = r_4 + 1

        mask = t_mask

        border = 5
        if r_1 > 1:
            mask[r_1: r_1 + border, :] = 0

        if r_2 > 1:
            mask[:, r_2: border + r_2] = 0

        if r_3 > 1:
            mask[mask.shape[0] - r_3 - border: mask.shape[0], :] = 0

        if r_4 > 1:
            mask[:, mask.shape[1] - r_4 - border:  mask.shape[1]] = 0
    return mask

def saveMaxNumberOfConnect(labels,type):
    vec = {}
    for i in range(0,labels.max()+1):
        vec[i]=np.sum(labels==i).astype(type)
    l = labels.max()
    del vec[0]
    matrix = np.zeros(labels.shape).astype(bool)
    for elemnt in vec.keys():
        matrix = np.logical_or(np.array(labels==elemnt).astype(bool),matrix)
    return matrix

def nthroot(x,n):
    y = (np.sign(x) + (x == 0)) * (np.abs(x) ** (1 / n))
    return y

def collaps(imageContain):
    Bn = []
    Bn.append(imageContain[0])
    numberOfLevel = len(imageContain)
    for n in range(1,numberOfLevel):
        size = (imageContain[n].size[0],
                imageContain[n].size[1])
        BnValL = np.array(imageContain[n])
        BnValR = np.array(Bn[n-1].resize(size,resample=Image.LANCZOS))
        Bn.append(Image.fromarray(BnValL+BnValR))
    if(DEBUG):
        fromContainerToPlot(Bn)
    return Bn[numberOfLevel-1]


def sigmoid(x):
    return 1.0 / (1 + np.exp(-100 * x))

def nonzeroPythonMatlab(x):
    index = np.nonzero(x)
    listOfNoneZero = []
    for i in range(0,len(index[0])):
        listOfNoneZero.append(x[index[0][i],index[1][i]])
    return listOfNoneZero

def maxk(tensor,k ,dim):
    indexesDim3 = np.argmax(tensor, axis=dim-1)
    returnTensor = np.zeros([tensor.shape[0],tensor.shape[1]])
    for i in range(0,tensor.shape[2]):
        boolMatric = np.array(i == indexesDim3).astype(int)
        returnTensor = returnTensor + tensor[:,:,i]*boolMatric
    return returnTensor,indexesDim3

def imageNR(im = None):
    # The NR eq is used in order to enhance the LF additive signal.
    # equstion 4
    n = 10
    m = 0
    c50 = 0.01
    imNR = im ** (n+m) / (im ** n + c50 ** n)
    return imNR

def LFsc(im = None,tat = None,Fac = None,debug=True):
    theta = tat * 180 / np.pi
    imNR = imageNR(im)
    # Returns a filter to approximate, once convolved with an image, the linear motion of a camera.
    # Fac specifies the length of the motion and theta specifies the angle of motion in degrees in a counter-clockwise direction.
    # The filter becomes a vector for horizontal and vertical motions.
    # The default len is 9 and the default theta is 0, which corresponds to a horizontal motion of nine pixels.
    #h = filterCretor(Fac,theta)
    h = fspecial(Fac,theta)
    #The LF is in fact a directional blurring in the direction of the filtering kernel.
    LF1  = 2*sc.signal.convolve2d(imNR,h,'same')
    #Shorter LF disance
    #h1 = line_create(np.round(Fac / 2), theta)
    #h1 = filterCretor(Fac/2, theta-180)
    h1 = fspecial(np.round(Fac / 2), theta)
    LF2 = sc.signal.convolve2d(imNR,h1,'same')
    #Summing long and short distance LF and normalizign accroding to Fac/4
    #out = max(Fac/4,1) * (LF1 + LF2);
    out = (LF1 + LF2) / 3
    return out,imNR

def gabor(sigma, theta, Lambda, psi, gamma):
    """Gabor feature extraction."""
    sigma_x = sigma
    sigma_y = float(sigma) / gamma

    # Bounding box
    nstds = 3  # Number of standard deviation sigma
    xmax = max(abs(nstds * sigma_x * np.cos(theta)), abs(nstds * sigma_y * np.sin(theta)))
    xmax = np.ceil(max(1, xmax))
    ymax = max(abs(nstds * sigma_x * np.sin(theta)), abs(nstds * sigma_y * np.cos(theta)))
    ymax = np.ceil(max(1, ymax))
    xmin = -xmax
    ymin = -ymax
    (y, x) = np.meshgrid(np.arange(ymin, ymax + 1), np.arange(xmin, xmax + 1))

    # Rotation
    x_theta = x * np.cos(theta) + y * np.sin(theta)
    y_theta = -x * np.sin(theta) + y * np.cos(theta)

    gb = np.exp(-.5 * (x_theta ** 2 / sigma_x ** 2 + y_theta ** 2 / sigma_y ** 2)) * np.cos(2 * np.pi / Lambda * x_theta + psi)
    return gb


def fromContainerToPlot(container):
    size = np.int(np.ceil(np.sqrt(container.__len__())))
    fig, ax = plt.subplots(size, size)
    fig.suptitle("")
    count = 0
    for i in range(0,size):
        for j in range(0, size):
            im = ax[i][j].imshow(normalize_image(container[count]))
            count+=1
            if count>=container.__len__():
                break
    plt.colorbar(im, ax=ax.ravel().tolist())
    plt.waitforbuttonpress()

def gabor3D(tetaRes, debug=False):
    cv2.getGaborKernel()
    # Calculate the number of 2 of the artical.
    # Gabor filter Calculation
    x0 = 8
    y0 = 8
    z0 = 8
    sig = 8
    lmd = 12
    [x, y, z] = np.meshgrid(range(1, 16), range(1, 16), range(1, 16))
    LCache = np.zeros([3,3,tetaRes])
    LnormCache = np.zeros([3,3,tetaRes])

def gabor1(tetaRes, debug=False):
    # Calculate the number of 2 of the artical.
    # Gabor filter Calculation
    x0 = 8
    y0 = 8
    sig = 8
    lmd = 12
    [x, y] = np.meshgrid(range(1, 16), range(1, 16))
    LCache = np.zeros([3,3,tetaRes])
    LnormCache = np.zeros([3,3,tetaRes])
    size = np.int(np.sqrt(tetaRes))
    if (debug):
        fig, ax = plt.subplots(size, size)
        fig.suptitle("gabor with {} orientation".format(tetaRes))
    count = 0
    line = 0
    for i in range(0, tetaRes):
        # Angles Calculations
        tat = np.pi * (i) / tetaRes

        # Gaussian envelope of the kernel
        exp = np.exp(-((x - x0) ** 2 / sig ** 2 + (y - y0) ** 2 / (sig) ** 2))

        LnormM = np.cos((2 * np.pi / lmd) * ((x - x0) * np.cos(tat) + (y - y0) * np.sin(tat)))
        Lnorm = Image.fromarray(LnormM)
        LnormCache[:,:,i] = Lnorm.resize(np.array([int(Lnorm.size[0] / 5), int(Lnorm.size[1] / 5)]), resample=Image.LANCZOS)

        L = LnormM*exp
        L = Image.fromarray(L)
        LCache[:,:,i] = L.resize(np.array([int(L.size[0] / 5), int(L.size[1] / 5)]), resample=Image.LANCZOS)

        if(debug):
            ax[line][count].imshow(exp*LnormM, cmap='jet')
            ax[line][count].set_title("orientation : {:.2f}".format(tat))
            count += 1
            if(count%size==0):
                count = 0
                line+=1
    if (debug):
        plt.waitforbuttonpress()

    return LnormCache,LCache

def showAarray(imageArray, NameOfImage, tetaRes):
    size = np.int(np.ceil(np.sqrt(imageArray.shape[2])))
    fig, ax = plt.subplots(size, size)
    fig.suptitle(NameOfImage)
    count = 0
    for i in range(0,size):
        for j in range(0,size):
            if(imageArray.shape[2]<=count):
                continue
            tat = np.pi * (count) / tetaRes
            val = ax[i][j].imshow(imageArray[:,:,count],cmap='jet')
            ax[i][j].set_title("orientation : {:.2f}".format(tat))
            count+=1
    fig.colorbar(val, ax=ax.ravel().tolist())
    plt.subplots_adjust(left=0.1,
                        bottom=0.1,
                        right=0.7,
                        top=0.9,
                        wspace=0.1,
                        hspace=0.5)


def LF(img, imageMask, tetaRes=1, imgRes=8, plotFlag=0,logLevel=0,maxType=max8bit,debug=False):
    debug2 = False
    cv2.destroyAllWindows()
    was_imgRes_char = 0
    stringType = "uint8"
    if maxType == max16bit:
        stringType="uint16"

    #if (img.shape[1 - 1] != 1024 or img.shape[2 - 1] != 1024):
    #    print('Resizing image to 1024X1024')
    #    I = cv2.resize(img, np.array([480, 480]), interpolation=cv2.INTER_LANCZOS4)

    # Constants per scale
    percentile_per_res = np.ones(imgRes)
    percentile_per_res = percentile_per_res * 95

    percentile_per_res[0:1] = 97

    percentile_per_res[2:3] = 95

    percentile_per_res[4:8] = 80

    m1 = 1
    m2 = 2

    # Res weight factor for sum
    tau = [14,12,10,8,6,4,2,1]
    tau = [1,2,4,8,16,32,64,128]
    #tau = [128,64,32,16,8,4,2,1]
    # Fac length per resulotion
    #Fac_array = [10,9,8,7,7,7,5,5]
    Fac_array = [10,9,8,7,7,7,5,5,5,5,4,4,4,3,3,3]
    # SE element radius per res

    #se_array = [0,2,3,4,5,5,5,6]
    se_array = [0,2,3,4,5,5,5,6,6,6,7,7,7,8,8,8]
    #se_array = np.ones(imgRes)*1
    gamma = imgRes


    # This is an array of images, containing the image for each resultion
    pyrmid_res = []

    im_res_array =  np.zeros([img.shape[0], img.shape[1], imgRes])
    im_res_mask_array =  np.zeros([img.shape[0], img.shape[1], imgRes])

    # The images of each scale will be summed into this image.
    T =  np.zeros([img.shape[0], img.shape[1]])

    # calculate a cache value
    LnormCache, LCache = gabor1(tetaRes)
    i = range(0,tetaRes)
    tatCache = np.pi * np.array(i).astype(float) / tetaRes

    #Iterate for each image scale
    start = time.time()
    for j in range(imgRes,0,-1):

        img = np.array(img)
        # The resize factors    for the row and column dimension of the image.
        RSfactorR = round(img.shape[0]/np.max([1,j]))
        RSfactorC = round(img.shape[1]/np.max([1,j]))
        # Instead of changing the kernel size, we   applied constant sized kernels on Gaussian   pyramid array of the input image
        img = Image.fromarray(img)
        imgS = img.resize(np.array([RSfactorR, RSfactorC]),resample=Image.LANCZOS)

        # This is an array of images, containing the image
        #for each orientation
        im_orientation_array =  np.zeros([np.size(imgS, 0), np.size(imgS, 1), tetaRes])
        cRF_p_array =  []

        # Iterate for each filter orientation
        cfrResult = 0
        if (debug):
            cfrResult = []
        for i in range(0,tetaRes):


            #Angles Calculations
            tat = tatCache[i]

            Lnorm = LnormCache[:,:,i]

            # L is the gabor kernel
            L = LCache[:,:,i]

            Lnorm = convolve2d (Lnorm, L, mode='same')

            # The normalization factor of the gabor kernel(1 / g)
            CONVnorm = Lnorm[int((L.shape[0] / 2)), int((L.shape[1] / 2))]

            # # Conv of the input image with the gabor oriented kernel
            # cRF represents the classical receptive field response, to which the LF will be added.
            # cRF = conv2(imgS, L, 'same') / CONVnorm
            cRF = cv2.filter2D(np.array(imgS), -1, np.array(L) ) / CONVnorm
            cRF = normalize_image(cRF)
            if debug:
                cfrResult.append(cRF)

            # img0(Eq. 1) contains both positive and negative values.
            # We will separate the image to two images, one with the positive values ( and zero instead negative values)
            # and the other with the negative values.
            # These two images will be considered differently with the following calculations,
            # as if they were obtained from two different kernels.
            cRF_p = np.maximum(cRF, 0)
            cRF_n = np.maximum(-cRF, 0)

            # All crf below 5
            # considered as noise. Zero it.
            THR = np.abs(cRF).max()
            cRF_p = ((cRF_p > 0.05).astype(int) * THR) * cRF_p
            cRF_n = ((cRF_n > 0.05).astype(int) * THR) * cRF_n
            cRF_p_array.append(cRF_p)
            # Michael Continue
            #Fac = max(5, 10 / j)
            Fac = Fac_array[j-1]

            #LF calc for the positive and negative images
            #LF
            [LF_p, cRF_p_nr] = LFsc(cRF_p, tat, Fac)
            [LF_n, cRF_n_nr] = LFsc(cRF_n, tat, Fac)
            LF_P_THR = np.abs(LF_p).max()
            LF_p = ((LF_p > 0.05).astype(int) * LF_P_THR) * LF_p
            LF_N_THR = np.abs(LF_n).max()
            LF_n = ((LF_n > 0.05).astype(int) * LF_N_THR) * LF_n

            LF_p = normalize_image(LF_p)
            LF_n = normalize_image(LF_n)

            #cRF_p_nr = normalize_image(cRF_p_nr)
            #cRF_n_nr = normalize_image(cRF_n_nr)

            # This is the details enhancement step(eq9 from hava dr.proposal)
            # max⁡(0, LFsignal - (const1 * NRsignal)) * const2
            # For high values of const1, the outcome would contain mostly facilitation from neighboring pixels.
            # For low values, the facilitation will be induced not only from the surrounding  but also from the signal  itself.
            # The aim of const2 component is to avoid invention of information and completion at areas without sub threshold
            # information
            sub_threshold_p = np.percentile(nonzeroPythonMatlab(cRF_p + LF_p), percentile_per_res[j-1], interpolation='midpoint')
            sub_threshold_n = np.percentile(nonzeroPythonMatlab(cRF_n + LF_n), percentile_per_res[j-1], interpolation='midpoint')
            alpha_p = 0
            alpha_n = 0

 #           LF_p = np.maximum(0, LF_p - alpha_p * cRF_p_nr) * sigmoid((cRF_p + LF_p) - sub_threshold_p)
  #          LF_n = np.maximum(0, LF_n - alpha_n * cRF_n_nr) * sigmoid((cRF_n + LF_n) - sub_threshold_n)

            if(False):
                val = plt.imshow(np.maximum(0, LF_p), cmap='jet')
                plt.suptitle("func".format(j))
                plt.colorbar(val)
                plt.waitforbuttonpress()
                plt.close()

            LF_p = np.maximum(0, LF_p) * sigmoid((cRF_p + LF_p) - sub_threshold_p)
            LF_n = np.maximum(0, LF_n) * sigmoid((cRF_n + LF_n) - sub_threshold_n)

            LF_p = normalize_image(LF_p)
            LF_n = normalize_image(LF_n)

            im_orientation_array[:,:, i] = -(0.5 * LF_n) + (0.5 * LF_p)
            im_orientation_array[:,:, i]= (np.abs(im_orientation_array[:,:, i])** m1)*np.sign(im_orientation_array[:,:, i])

            #end
        # Summing of the orientation responses per resolution Take only the repsonse from the leading orientation.

        # T_j = max(im_orientation_array, [], 3)
        if(debug2):
            fromContainerToPlot(cRF_p_array)
            fromContainerToPlot(cfrResult)


        if (debug2):
            showAarray(im_orientation_array,"",tetaRes)
            plt.waitforbuttonpress()

        # orientation max

        T_j,orintation = maxk(im_orientation_array, 1, 3)

        if (debug2):
            val = plt.imshow(orintation, cmap='jet')
            plt.suptitle("Max Of Orientations Level {}".format(j))
            plt.colorbar(val)
            plt.waitforbuttonpress()
            plt.close()
            val = plt.imshow(T_j,cmap='jet')
            plt.suptitle("Max Of Orientations Level {}".format(j))
            plt.colorbar(val)
            plt.waitforbuttonpress()
            plt.close()


        if (T_j.shape.__len__()>2):
            T_j = np.median(T_j, 3)

        T_j = (np.abs(T_j) ** 1 / m1) * np.sign(T_j)

        # Resizing back to the original image size.
        T_j = Image.fromarray(T_j)
        pyrmid_res.append(T_j)
        im_res_array[:, :, j-1] = np.array(T_j.resize(np.array([int(img.size[0]), int(img.size[1])]), resample=Image.LANCZOS))
        # Summing the results of each image scale
        # Computing the BVmask

        val = im_res_array[:, :, j-1]
        val = val - val.min()
        tensorThresh = (val * maxType).astype(stringType)
        ret, BVmask = cv2.threshold(tensorThresh,0,1,cv2.THRESH_OTSU)
        if debug2:
            cv2.imshow("mask", BVmask*maxType)
            cv2.waitKey(10)

        ## Morphological Edits to mask
        se_rad = se_array[j-1]
        se = morphology.disk(se_rad)
        BVmask = cv2.morphologyEx(BVmask, cv2.MORPH_CLOSE, se)
        BVmask = cv2.morphologyEx(BVmask, cv2.MORPH_ERODE, se)

        # oldMaskWorker(mask)

        num_labels, all_labels = cv2.connectedComponents(BVmask,
                                                     connectivity=8)
        temp_mask = BVmask
        if logLevel>1:
            cv2.imshow("layer",temp_mask*maxType)
            cv2.waitKey(10)
        temp_mask = saveMaxNumberOfConnect(all_labels,"int32")
        mask = temp_mask.astype(stringType)
        if logLevel>1:
            cv2.imshow("new layer",mask*maxType)
            cv2.waitKey(10)
        im_res_mask_array[:,:, j-1] = mask
        #im_res_array[:,:, j-1] = im_res_array[:,:, j-1] / 2 ** (sigmoid(abs(j - 2)))
        # Weight each resolution differently
        T = T + im_res_array[:,:, j-1] / 2 ** (sigmoid(abs(j - 2)))#im_res_array[:,:, j-1]

        if debug2:
            plt.imshow(T)
            plt.waitforbuttonpress()
            plt.close()

    #collapsImage = collaps(pyrmid_res)
    #T = np.array(T.resize(np.array([int(img.size[0]), int(img.size[1])]), resample=Image.LANCZOS))
    if (False):
        plt.imshow(normalize_image(collapsImage))
        plt.waitforbuttonpress()
        plt.close()
    end = time.time()
    print(end - start)

    im_res_arrayOut = im_res_array
    im_res_mask_arrayOut = im_res_mask_array

    maskOut = np.zeros(mask.shape);

    maskOut = np.sum(im_res_mask_array,axis=2)
    idxOut = abs(maskOut) > 0
    skeleton = skeletonize(idxOut).astype(stringType)



    # maskOut = nthroot(maskOut, 3)

    maskOut = maskOut * idxOut * imageMask

    T = T * (imageMask)

    # Whole image out treatment # formula 24
    T_tag = (1 / gamma) * (np.abs(T) ** 1 / m2) * np.sign(T)
    T_tag = T_tag - T_tag.min()
    imgOut = Image_normalize_image(T_tag)
    maskOut = Image_normalize_image(maskOut)
    return imgOut, maskOut, im_res_arrayOut, im_res_mask_arrayOut

def main():
    tetaRes = 25
    counter = 12
    fig, ax = plt.subplots(5, 5)
    for i in range(0, 5):
        for j in range(0, 5):
        # Angles Calculations
            tat = np.pi * (i) / tetaRes
            theta = 90 - tat * 180 / np.pi
            h = fspecial('motion',100,theta)
            counter+=1
            ax[i][j].imshow(h,cmap="gray")
    plt.show()
if __name__ == '__main__':
    main()
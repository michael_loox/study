import glob
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np


def saveResult(imageToSave, location="", type="jpg"):
    if location == "":
        assert ("error")
    print("MakeLocation {}".format(os.path.dirname(location)))
    os.makedirs(os.path.dirname(location), exist_ok=True)
    file = location + "."+type
    print("savingResult {}".format(file))
    if type == "jpg":
        cv2.imwrite(file, imageToSave.astype(np.uint8))
    else:
        with open(file, 'wb') as f:
            np.save(f, np.array(imageToSave))

def filterCretor(lineLength=20, p3=0, distanec=0):
    mu = 0
    sigma = 3
    # Line creator
    filterImage = np.zeros([int(lineLength), int(lineLength)], dtype=np.float)
    phi = p3 / 180 * np.pi
    cosphi = np.cos(phi)
    sinphi = np.sin(phi)
    pt1 = (int(lineLength / 2), int(lineLength / 2))
    pt2 = (pt1[0] + int(2 * lineLength * cosphi), pt1[1] + int(2 * lineLength * sinphi))
    filterImage = cv2.line(filterImage, pt1, pt2, 1)
    # Apply normal distribute on the filter.
    lastpix = np.where((filterImage == 1))
    half = int(len(lastpix[0]) / 2)
    if(p3<0):
        [x] = np.meshgrid(range(-half, 0, 1))
    else:
        [x] = np.meshgrid(range(0, half, 1))
    value = 1 / (sigma * np.sqrt(2 * np.pi))
    value = value * np.exp(-0.5 * ((x - mu) / sigma) ** 2)
    #if(pt2[0]<pt1[0]):
    #    value = value[::-1]
    if len(lastpix[0]) >= len(value):
        pad = np.zeros(len(lastpix[0]) - len(value))
        if (p3 > 0):
            filterImage[lastpix] = np.append(value, pad)
        if (p3 < 0):
            filterImage[lastpix] = np.append(pad, value)
    if len(lastpix[0]) < len(value):
        filterImage[lastpix] = value[len(value)-len(lastpix[0]):]
    #out1 = cv2.resize(filterImage, [200, 200])
    # cv2.imshow("new",filterImage)
    #plt.plot(x,value, color="red")
    #plt.show()
    #plt.waitforbuttonpress()
    return filterImage


def fspecial(p2=9, p3=0, radius=0, len=9, angle=0):
    eps = 2.2204e-16
    len = np.max([1, p2])
    half = (len - 1) / 2  # rotate
    # half length around center
    phi = np.mod(p3, 180) / 180 * np.pi

    cosphi = np.cos(phi)
    sinphi = np.sin(phi)
    xsign = np.sign(cosphi)
    linewdt = 1

    # define mesh for the half matrix, eps takes care of the right size
    # for 0 & 90 rotation
    sx = np.fix(half * cosphi + linewdt * xsign - len * eps)
    sy = np.fix(half * sinphi + linewdt - len * eps)
    [x, y] = np.meshgrid(range(0, int(sx) + int(xsign), int(xsign)), range(0, int(sy) + 1, 1))

    # define shortest distance from a pixel to the rotated line
    dist2line = (y * cosphi - x * sinphi);  # distance
    # perpendicular to the line

    rad = np.sqrt(x ** 2 + y ** 2)
    # find points beyond the line
    # s end-point but within the line width
    lastpix = np.where((rad >= half) & (np.abs(dist2line) <= linewdt))
    # distance to the line
    # s end-point parallel to the line
    x2lastpix = half - np.abs((x[lastpix] + dist2line[lastpix] * sinphi) / cosphi)

    dist2line[lastpix] = np.sqrt(dist2line[lastpix] ** 2 + x2lastpix ** 2)
    dist2line = linewdt + eps - np.abs(dist2line)
    dist2line[dist2line < 0] = 0  # zero out anything beyond line width

    # unfold half - matrix to the full size
    h = cv2.rotate(dist2line, cv2.ROTATE_180)
    end0 = h.shape[0]
    end1 = h.shape[1]
    h = np.pad(h, [0, np.max([end0, end1])])
    v = h[end0 + np.array(range(1, end0 + 1)) - 2, :]
    v = v[:, end1 + np.array(range(1, end1 + 1)) - 2]
    if (end1 == 1):
        v = np.reshape(v, [v.shape[0], 1])
    v = dist2line
    h1 = 2 * end0 - 3
    if (2 * end1 - 2) == 0:
        h1 = 1
    h = h[0:2 * end0 - 3, 0:h1]
    h = h / (np.sum(h) + eps * len * len)
    if cosphi > 0:
        h = np.flipud(h)
    return h


def Image_normalize_image(matrix):
    matrix = (matrix - np.min(matrix)) / (np.max(matrix) - np.min(matrix))
    return matrix


def normalize_image(matrix):
    matrix = (matrix) / (np.max(matrix) - np.min(matrix))
    return matrix


def sortImageList(listOfFile):
    sortFile = [None] * 25
    for file in listOfFile:
        sortFile[int(file[str(file).find("DN") + 2:str(file).find("heal")])] = file
    return sortFile


def runOverImage(path, filter = "", type="npy"):
    files = glob.glob(os.path.abspath(path) + "\\**\\*."+ type, recursive=True)
    listOfFiles = []
    for file in files:
        if len(filter) > 0 and str(file).count(filter):
            listOfFiles.append(file)
        if len(filter) == 0:
            listOfFiles.append(file)
        # files.append(os.path.join(r, file))
    return listOfFiles


def getNormilized(image):
    return cv2.normalize(image, dst=None, alpha=0, beta=1,
                         norm_type=cv2.NORM_MINMAX).astype(float)


def AbsTeta(length=10, theta=0):
    phiX = 1.5
    phiY = 0.25
    length = int(length / 2)
    [x, y] = np.meshgrid(range(-length, length), range(-length, length))
    a = 1 / (2 * (phiX * phiY))
    phi = np.mod(theta, 180) / 180 * np.pi
    xtag = x * np.cos(phi) + y * np.sin(phi)
    ytag = y * np.cos(phi) - x * np.sin(phi)
    Ads = a * np.exp(0.5 * (xtag / phiX + ytag / phiY))
    return Ads


def Abs(tetaRes=8, length=10):
    phiX = 1.5
    phiY = 0.25
    [x, y] = np.meshgrid(range(-length, length), range(-length, length))
    a = 1 / (2 * (phiX * phiY))
    for i in range(0, tetaRes):
        tat = np.pi * (i) / tetaRes
        theta = 90 - tat * 180 / np.pi
        xtag = x * np.cos(theta) + y * np.sin(theta)
        ytag = y * np.cos(theta) - x * np.sin(theta)
        Ads = a * np.exp(0.5 * (xtag / phiX + ytag / phiY))
        cv2.imshow("new", Ads)
        plt.imshow(Ads, cmap="jet")
        plt.waitforbuttonpress()


def main():
    tetaRes = 8
    counter = -8
    length = 20
    [x] = np.meshgrid(range(-length, length, 1))
    fig, ax = plt.subplots(4, 4)
    for i in range(0, 4):
        for j in range(0, 4):
            # Angles Calculations
            tat = np.pi * (counter) / tetaRes
            theta = tat * 180 / np.pi
            theta = theta
            # h = fspecial(100, theta)
            h = line_create(length, theta)
            counter += 1
            val = ax[i][j].imshow(h, cmap="gist_ncar",extent=[-length,length,-length,length])
            ax[i][j].set_title("orientation : {:.2f}".format(theta))
    fig.colorbar(val, ax=ax.ravel().tolist())
    fig.suptitle("Line Gaussian filter with {} orientations filter size {}".format(tetaRes,2*length))
    plt.subplots_adjust(left=0.1,
                        bottom=0.1,
                        right=0.7,
                        top=0.9,
                        wspace=0.1,
                        hspace=0.5)
    plt.show()
    return


if __name__ == '__main__':
    main()

import os
import cv2
from utill import runOverImage
import numpy as np
import matplotlib.pyplot as plt
from skimage.segmentation import flood, flood_fill
from skimage.transform import hough_circle, hough_circle_peaks
from skimage import morphology
from collections import Counter
from parabola_detector import parabolaEnter
import random

def sigmoid(a,b,image,debug=False):
    '''

    :param a: slip of the function
    :param b: the center of the function
    :param image: input matrix
    :param debug: print the result of the function
    :return: apply the function to the image and return new image.
    '''
    #todo: Apply a and b automatic process.
    if debug:
        print(np.max(image))
        print(np.min(image))
    s = 1/(1+np.exp(-a*(image-b)))
    if debug:
        print(np.max(s))
        print(np.min(s))
    if debug:
        orignal = cv2.applyColorMap((image * 255).astype('uint8'), cv2.COLORMAP_JET)
        cv2.imshow("Original temp", orignal)
        v = cv2.applyColorMap((s*255).astype('uint8'), cv2.COLORMAP_JET)
        cv2.imshow("contrastMap",v)
    return s

def YUV(image,n,tau,debug=False):
    image3d = np.zeros([image.shape[0],image.shape[1],3])
    image3d[:,:,0] = image
    image3d[:, :, 1] = np.sinh(image) * np.cos(2 * np.pi * n * (image - tau))
    image3d[:, :, 2] = np.sinh(image) * np.sin(2 * np.pi * n * (image - tau))
    if debug:
        cv2.imshow("YUV",image3d)

def contrastMap(image,debug=False):
    '''
    :param image: nipple get hdr image after processing.
    :return: return location of the nipple
    '''
    if(debug):
        cv2.imshow("original", image)
    # stage a: apply sigmoid function on the image
    contrastImage = sigmoid(7,0.6,image)
    # stage b: Convert image to the Yuv
    YUV(contrastImage,5,0.5)
    return contrastImage

def Armpit(binImage,side,debug=False):
    if debug:
        cv2.imshow("new", binImage)
    val = np.column_stack(np.where(binImage==255))
    listVal = {}
    max = side*binImage.shape[0]
    if side == 1:
        val = val[::-1]
    for i in range(np.min(val[:,0]),int(np.max(val[:,0])/2)):
        if side==0:
            if (max<val[val[:,0].tolist()[i],1]):
                max = val[val[:,0].tolist()[i],1]
                index = i
        else:
            if (max > val[val[:, 0].tolist()[i], 1]):
                max = val[val[:, 0].tolist()[i], 1]
                index = i
    return index
def nippleDetection1(imageCopy, imageC, maskVal,side,debug=False):
    image = cv2.copyTo(imageCopy,mask=np.ones(imageCopy.shape).astype("uint8"))
    Ed  = cv2.Canny(image,20,50)
    Edc = cv2.Canny(imageC,60,90)
    setVal = np.zeros(image.shape)
    setVal[np.ceil(image.shape[0]*3/7).astype(int):,:]=1
    bitWiseMapNipple = cv2.bitwise_and(Ed, Edc, mask=(setVal*maskVal).astype(np.uint8()))


    hough_radii = np.arange(10, 30, 5)
    hough_res = hough_circle(bitWiseMapNipple, hough_radii)
    accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                               total_num_peaks=10)
    # location = 2
    # cropValues = []
    # loactionS = []
    # vote = np.zeros(image.shape)
    # for (x, y, r) in zip(cx, cy, radii):
    #     sizeValue = np.array(image[x - location:x + location, y - location:y + location]).shape
    #     if(sizeValue[0]*sizeValue[1])==16:
    #         cropValues.append(np.average(image[y - r:y + r,x - r:x + r]))
    #         loactionS.append([x, y, r])
    #         vote[y - r:y + r,x - r:x + r] = vote[y - r:y + r,x - r:x + r]+1
    #
    # location = np.unravel_index(vote.argmax(), vote.shape)
    # val = np.argmin(cropValues)
    # for (x, y, r) in zip(cx, cy, radii):
    #     # draw the circle in the output image, then draw a rectangle
    #     # corresponding to the center of the circle
    #     image2 = cv2.circle(image, (x, y), r, (0, 255, 0), 4)
    #
    # y,x= location
    (x, y, r)  =  zip(cx, cy, radii)
    #   r = 10
    image1 = cv2.circle(image, (x, y), r, (0, 255, 0), 4)
    crop = image1[x - r:x + r, y - r:y + r]
    valRetrun = image1

def nippleDetection1(imageCopy, imageC, maskVal,side,debug=False):
    image = cv2.copyTo(imageCopy,mask=np.ones(imageCopy.shape).astype("uint8"))
    Ed  = cv2.Canny(image,20,50)
    Edc = cv2.Canny(imageC,60,90)
    setVal = np.zeros(image.shape)
    setVal[np.ceil(image.shape[0]*1/3).astype(int):,:]=1
    bitWiseMapNipple = cv2.bitwise_and(Edc, Ed, mask=(setVal*maskVal).astype(np.uint8()))


    hough_radii = np.arange(10, 30, 5)
    hough_res = hough_circle(bitWiseMapNipple, hough_radii)

    # Select the most prominent 3 circles
    accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                               total_num_peaks=1)
    crop = np.ones([20,20])
    valRetrun = image
    for (x, y, r) in zip(cx, cy, radii):
        # draw the circle in the output image, then draw a rectangle
        # corresponding to the center of the circle
        image1 = cv2.circle(image, (x, y), r, (0, 255, 0), 4)
        crop = image1[y - r:y + r,x - r:x + r]
        valRetrun = image1

    if (debug):
        fig, ax = plt.subplots(2, 2)
        ax[0][0].set_title("Ed")
        ax[0][0].imshow(Ed, cmap='gray')
        ax[0][1].set_title("Edc")
        ax[0][1].imshow(imageCopy, cmap='gray')
        ax[1][0].set_title("image2")
        ax[1][0].imshow(bitWiseMapNipple, cmap='gray')
        ax[1][1].set_title("nipple")
        ax[1][1].imshow(image1, cmap='gray')
        fig.show()
        plt.waitforbuttonpress()
        plt.close('all')

    if crop.size == 0:
        return np.ones([20,20]), valRetrun
    return crop, valRetrun

def houghparabola(BW):
    halfImage = [None]
    E = BW
    beginX = 0
    endX = int(BW.shape[1])
    E = (E)[:, beginX: endX]
    Index = np.argwhere(E == 255)
    rvals = np.arange(0.01, 0.2, 0.01)
    Idy = np.array(Index[:, 0]).reshape(Index[:, 0].size, 1)
    Idx = np.array(Index[:, 1]).reshape(Index[:, 0].size, 1)
    [N, M] = np.array(E).shape
    R = rvals.size
    ACC_MAT = np.zeros((N, M, R), np.uint8)
    x0_v = np.arange(0, M, 1)
    for r in range(0, R):
        for x0 in x0_v:
            Y0 = Idy + rvals[r] * (Idx - x0) ** 2
            Y0 = Y0[(Y0 > 250) & (Y0 < N)].astype(int)
            Y0_count = Counter(Y0)
            for y0 in Y0_count:
                ACC_MAT[y0, x0, r] += Y0_count[y0]

    maxValue = np.argwhere(np.amax(ACC_MAT) == ACC_MAT)
    y0_v_max = maxValue[:, 0]
    x0_v_max = maxValue[:, 1]
    r0_v_max = maxValue[:, 2]
    print(r0_v_max)
    matrix = np.zeros((E.shape[0], E.shape[1], 3)).astype(np.int)
    x_v = np.array(range(0, M)).reshape(M, 1)
    count = 1

    # for y0 in y0_v_max:
    #     for x0 in x0_v_max:
    #         for r0 in r0_v_max:
    area = np.zeros([len(y0_v_max),len(x0_v_max),len(r0_v_max)])
    ar = np.zeros(BW.shape)
    #for index in range(0,len(y0_v_max))
    xc,yc,rc = 0,0,0
    for y0 in y0_v_max:
        xc=0
        for x0 in x0_v_max:
            rc = 0
            for r0 in r0_v_max:
                y_v = np.round(y0 - rvals[r0] * (x_v - x0) ** 2)
                point1 = np.concatenate((x_v, y_v), axis=1).astype(
                    np.int)

                point1 = point1[(point1[:, 0] >= 0) & (point1[:, 0] < N)]
                matrix = cv2.circle(matrix, (x0, y0), 5,
                                    count, 1)
                cv2.polylines(matrix, [point1.astype(int)], True,
                              (count, 0, 0))
                data = cv2.fillConvexPoly(ar, point1, 1)
                area[xc,yc,rc] = np.sum(data == 1)
                count += 1
                rc+=1
            xc += 1
        yc += 1
    valIndex = np.unravel_index(np.argmax(area, axis=None), area.shape)
    x0 = x0_v_max[valIndex[0]]
    y0 = y0_v_max[valIndex[1]]
    r0 = r0_v_max[valIndex[2]]
    y_v = np.round(y0 - rvals[r0] * (x_v - x0) ** 2)
    point1 = np.concatenate((x_v, y_v), axis=1).astype(
        np.int)

    if False:
        halfImage = matrix[:, :, 0]
        grayRGB = np.zeros([BW.shape[0], BW.shape[1], 3])
        grayRGB[:, :, 0] = BW;
        grayRGB[:, :, 1] = BW;
        grayRGB[:, :, 2] = BW;
        maskVal = halfImage
        maskValIntNot = np.bitwise_not(maskVal.astype(np.bool)).astype(np.uint8)
        grayRGB[:, :, 1] = grayRGB[:, :, 1] * maskValIntNot
        grayRGB[:, :, 2] = grayRGB[:, :, 2] * maskValIntNot
        grayRGB[:, :, 0] = grayRGB[:, :, 0] * maskValIntNot
        # rgb 001 010 100
        maskVal = maskVal.astype(np.uint8)
        grayRGB[:, :, 0] += (maskVal.astype(np.uint8) % 2 == 1) * 255
        maskVal = np.right_shift(maskVal, 2)
        grayRGB[:, :, 1] += ((maskVal.astype(np.uint8) % 2) == 1) * 255
        maskVal = np.right_shift(maskVal, 2)
        grayRGB[:, :, 2] += ((maskVal.astype(np.uint8) % 2) == 1) * 255
        cv2.imshow("dsa", grayRGB)
        cv2.waitKey()
    ar = np.zeros(BW.shape)
    ar = cv2.fillConvexPoly(ar, point1, 1)
    return (x0, y0, ar)

def workingCounter(imageCounter):
    img = imageCounter[0]
    img_blur = cv2.GaussianBlur(img, (7, 7), 0)
    edges = cv2.Canny(image=img_blur, threshold1=50, threshold2=100)  # Canny Edge Detection
    zeroImage = np.zeros(np.shape(edges)).astype(np.uint8)
    h, w = edges.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)
    index = np.where(zeroImage == 0)
    intanst = np.zeros((h , w ), np.uint8)
    temp = intanst
    im_flood_fill = edges.copy()
    temprue = []
    while len(index[0]) > 1:
        print(len(index[0]))
        index1 = random.randint(0,len(index[0])-1)
        print(index1)
        y_loc  = index[0][index1]
        x_loc = index[1][index1]
        cv2.floodFill(im_flood_fill, mask, (x_loc,y_loc), 255)
        im_flood_fill[y_loc,x_loc] = 255
        working = np.logical_not(zeroImage)*im_flood_fill
        zeroImage = np.bitwise_or(zeroImage,(im_flood_fill/255).astype(np.int))
        value = np.sum(imageCounter[1]*working/255)/np.sum(working/255)
        temprue.append(value)
        intanst = intanst+working/255*value
        # plt.imshow(intanst, cmap='jet')
        # plt.show()
        index = np.where(zeroImage == 0)
    return intanst,temprue

def nippleDetection3(imageCopy, imageC, maskVal,side,debug=False):
    image = cv2.copyTo(imageCopy,mask=np.ones(imageCopy.shape).astype("uint8"))
    level = 6
    imageCounter = []
    imageCounter.append(image)
    crop = None
    w,h = image.shape
    vote = np.zeros(image.shape)
    for i in range(0,level):
        imageCounter.append(cv2.resize(cv2.pyrDown(imageCounter[i]),[h,w]))

    newImage,temprue = workingCounter(imageCounter)
    newImage[0:int(h/4),:] = 0
    temprue = np.sort(temprue)[1:]
    temprue = temprue[-1:]
    out=0
    while len(temprue)>0:
        mask = newImage==temprue[0]
        if(np.sum(mask*1)>1):
            break
        else:
            temprue = np.sort(temprue)[1:]
    image = cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
    color = np.array([0, 0, 255], dtype='uint8')
    masked_img = np.where(mask[:,:,np.newaxis], color, image)
    out = cv2.addWeighted(image, 0.2, masked_img, 0.8, 0)
    return newImage, out

def SORF(imageCounter = [] , mask = []):
    if( mask==[] ):
        mask = np.ones([imageCounter[0].shape])
    Ed = []
    Ed.append(cv2.Canny(imageCounter[0], 0, 30))
    level = np.shape(imageCounter)[0]
    vote = np.zeros(imageCounter[0].shape).astype(float)
    for i in range(1,level-1):
        # ret, BVmask = cv2.threshold(imageCounter[i], 0, 1, cv2.THRESH_OTSU)
        # se = morphology.disk(3)
        # BVmask = cv2.morphologyEx(BVmask, cv2.MORPH_ERODE, se)
        temp = cv2.Canny(imageCounter[i], 0, 30).astype(float)*mask

        vote = vote + temp ** 2
    vote = vote / level
    vote = vote ** (1 / 2)
    return vote > 0
def nippleDetection(imageCopy, imageC, maskVal,side,debug=False):
    image = cv2.copyTo(imageCopy,mask=np.ones(imageCopy.shape).astype("uint8"))
    level = 6
    imageCounter = []
    imageCounter.append(image)
    crop = None
    w,h = image.shape
    vote = np.zeros(image.shape)
    for i in range(0,level):
        imageCounter.append(cv2.resize(cv2.pyrDown(imageCounter[i]),[h,w]))
    kernel = np.ones((10, 10), np.uint8)
    newMaskValue = cv2.erode(maskVal, kernel, iterations=1)
    voteMult = SORF(imageCounter,newMaskValue)
    if(debug):
        with open('tests/imageSurf.npy', 'wb') as f:
            np.save(f, imageCounter)
        fig, ax = plt.subplots(2, 3)
        c = 0
        for i in range(0,3):
            for j in range(0,2):
                Ed = cv2.Canny(imageCounter[c],0,30)
                ax[j][i].imshow(Ed)
                ax[j][i].set_title("Layer{}".format(c))
                c+=1
        plt.tight_layout(pad=0.00)
        plt.show()
        plt.waitforbuttonpress()
    hough_radii = np.arange(10, 50, 5)

    if True:
        hough_res = hough_circle(voteMult, hough_radii)
        accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                                   total_num_peaks=10)
        for (x, y, r) in zip(cx, cy, radii):
            if x < 2 or y < 2:
                continue
            if (x < r):
                r = x - 1
            if (y < r):
                r = y - 1
            vote[y - r:y + r, x - r:x + r] = vote[y - r:y + r, x - r:x + r]+1

    else:
        for i in range(2,level-1):
            # ret, BVmask = cv2.threshold(imageCounter[i], 0, 1, cv2.THRESH_OTSU)
            # se = morphology.disk(3)
            # BVmask = cv2.morphologyEx(BVmask, cv2.MORPH_ERODE, se)
            Ed = cv2.Canny(imageCounter[i], 0, 30)
            setVal = np.zeros(image.shape)
            setVal[np.ceil(image.shape[0] * 1 / 3).astype(int):, :] = 1
            Ed = cv2.bitwise_and(Ed, Ed, mask=(setVal * maskVal).astype(np.uint8()))

            hough_res = hough_circle(Ed, hough_radii)
            accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                                       total_num_peaks=10)
            for (x, y, r) in zip(cx, cy, radii):
                if x < 2 or y < 2:
                    continue
                if(x<r):
                    r = x-1
                if(y<r):
                    r = y-1
                maxValue = np.max(voteMult[y - r:y + r, x - r:x + r])
                vote[y - r:y + r, x - r:x + r] = vote[y - r:y + r, x - r:x + r]+maxValue
    if False:
        out = cv2.copyTo(imageCopy,mask=np.ones(imageCopy.shape).astype("uint8"))
        plt.imshow(out+vote)
        plt.show()
        plt.waitforbuttonpress()

    if(vote.max()<= 2):
        return crop, image
    val = vote.max()-2<=vote
    image = cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)

    segment = np.zeros(image.shape[:2], np.uint8)
    segment[val] = 1

    background_mdl = np.zeros((1,65), np.float64)
    foreground_mdl = np.zeros((1,65), np.float64)
    try:
        segment, bgdModel, fgdModel = cv2.grabCut(image, segment, None, background_mdl, foreground_mdl, 5,
                    cv2.GC_INIT_WITH_MASK)
    except:
        print("error")
    new_mask = np.where((segment == 2) | (segment == 0), 0, 1).astype('uint8')

    m = cv2.moments(new_mask, True);
    try:
        p = [int(m['m10'] / m['m00']), int(m['m01'] / m['m00'])]
        cv2.circle(new_mask, p, 5, (128, 0, 0), -1)
    except:
        print("error")
    num_labels, labels = cv2.connectedComponentsWithAlgorithm(new_mask,
                                                              connectivity=4, ltype=cv2.CV_16U, labels=2,
                                                              ccltype=cv2.CCL_DEFAULT)
    labelsName = np.unique(labels)
    size = [0] * labelsName.size
    for label in labelsName:
        if label == 0:
            continue
        size[label] = np.sum(labels[labels == label]) / label
    correct_label = np.argwhere(np.max(size) == size)
    labels[labels != correct_label] = 0
    labels[labels == correct_label] = 1
    new_mask = labels
    color = np.array([0, 0, 255], dtype='uint8')
    masked_img = np.where(new_mask[:,:,np.newaxis], color, image)
    out = cv2.addWeighted(image, 0.2, masked_img, 0.8, 0)

    if(debug):
        fig, ax = plt.subplots(1,3)
        ax[0].set_title("Orginal Image")
        ax[0].imshow(imageCopy, cmap='gray')
        ax[1].set_title("SORF signal Image")
        ax[1].imshow(voteMult, cmap='jet')
        ax[2].set_title("Result")
        ax[2].imshow(out)
        plt.waitforbuttonpress()

    return crop, out


def slidingWindows(image, knn,size=30,stepSize=10):
    #image = cv2.resize(image, (400, 400))
    image = np.floor(image*255).astype(np.uint8())
    tmp = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)  # for drawing a rectangle
    (w_width, w_height) = (size, size)  # window size
    for x in range(0, image.shape[1] - w_width, stepSize):
        for y in range(0, image.shape[0] - w_height, stepSize):
            image1 = image[x:x + w_width, y:y + w_height]
            ## let's see what the image looks like
            #image = cv2.cvtColor(window, cv2.COLOR_BGR2RGB)
            if(image1.shape[0]==0 or image1.shape[1]==0):
                continue
            my_image = cv2.resize(image1, (32, 32))
            cv2.imshow("new",my_image)
            pixel_image = my_image.flatten()
            pixel_image = np.array([pixel_image]).astype('float32')
            ret, result, neighbours, dist = knn.findNearest(pixel_image, k=10)
            # classify content of the window with your classifier and
            # determine if the window includes an object (cell) or not
            # draw window on image
            if(int(ret)==1):
                color = (255, 0, 0)
            else:
                cv2.rectangle(tmp, (x, y), (x + w_width, y + w_height), (255, 0, 0), 2)  # draw rectangle on image
                print("find")
                plt.imshow(np.array(tmp).astype('uint8'))
    print("ok")
    plt.show()
    plt.waitforbuttonpress()

def nippleUsingKnn(imageCopy):
    ## get the model from the cv studio storage
    fs = cv2.FileStorage('C:\\study\\study\\DeepLearning\\knn_samples.yml', cv2.FILE_STORAGE_READ)
    knn_yml = fs.getNode('opencv_ml_knn')

    knn_format = knn_yml.getNode('format').real()
    is_classifier = knn_yml.getNode('is_classifier').real()
    ## number of samples
    default_k = knn_yml.getNode('default_k').real()
    ## sample arrays
    samples = knn_yml.getNode('samples').mat()
    ## labels of sample
    responses = knn_yml.getNode('responses').mat()
    fs.release
    knn = cv2.ml.KNearest_create()
    knn.train(samples, cv2.ml.ROW_SAMPLE, responses)
    #my_image = cv2.resize(imageCopy, (32, 32))
    #pixel_image = my_image.flatten()
    #pixel_image = np.array([pixel_image]).astype('float32')
    #ret, result, neighbours, dist = knn.findNearest(pixel_image, k=3)
    #print("status:"+str(ret))
    slidingWindows(imageCopy,knn)
    return

def createAnnotations():
    listNippleM = runOverImage("images\\nipple")
    listNotNippleM = runOverImage("images\\notNipple")
    dictList = {"annotations":{},"labels":[]}
    dictList["labels"].append("nipple")
    dictList["labels"].append("NotNipple")
    for item in listNippleM:
        dictList["annotations"][item]={"label":"nipple"}
    for item in listNotNippleM:
        dictList["annotations"][item]={"label":"NotNipple"}
    return dictList



def main():
    files = []
    path2 = "../data/sick"
    path1 = "../data/healthy/900"
    #path1 = "../data/sick/263"
    path = "../data/"
    saveNipplePath = "../nippleFirstSign"

    mode = 'hdr'
    files = runOverImage(path1)
    for i in files:
        image = np.loadtxt(i)
        rgb_out = cv2.normalize(image, None, 0, 1,
                                cv2.NORM_MINMAX).astype(float)
        imgC = contrastMap(rgb_out,True)
        rgb_out_C = cv2.normalize(image, None, 0, 255,
                                cv2.NORM_MINMAX).astype(np.uint8())
        imgC_C = cv2.normalize(imgC, None, 0, 255,
                                  cv2.NORM_MINMAX).astype(np.uint8())
        nippleDetection(rgb_out_C,imgC_C,maskVal=np.ones(rgb_out_C.shape),side=0,debug=True)

        A = cv2.waitKey(0)

if __name__ == "__main__":
    main()
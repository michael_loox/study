# importing the libraries
from torchsummary import summary
import pandas as pd
import numpy as np
from skimage.io import imread, imsave
from tqdm import tqdm
import os
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from skimage.transform import rotate
from skimage.util import random_noise
from skimage.filters import gaussian
from scipy import ndimage

# loading dataset
data = pd.read_csv('C:\\Teza\\study\\final\\ImageTest\\dataset\\hdr\\database.csv')
data.head()
imagePathValue = "C:\\Teza\\study\\final\\ImageTest\\dataset\\hdr"
train_img = []
for index, img_name in tqdm(data.iterrows()):
    image_path = os.path.join(imagePathValue,img_name["statusString"],img_name["image_names"])
    img = imread(image_path)
    img = img/255
    train_img.append(img)

train_x = np.array(train_img)
train_y = data['status'].values
train_x.shape, train_y.shape

train_x, val_x, train_y, val_y = train_test_split(train_x, train_y, test_size = 0.1, random_state = 13, stratify=train_y)
(train_x.shape, train_y.shape), (val_x.shape, val_y.shape)


final_train_data = []
final_target_train = []
for i in tqdm(range(train_x.shape[0])):
    final_train_data.append(train_x[i])
    final_train_data.append(rotate(train_x[i], angle=45, mode = 'wrap'))
    final_train_data.append(np.fliplr(train_x[i]))
    final_train_data.append(np.flipud(train_x[i]))
    final_train_data.append(random_noise(train_x[i],var=0.2**2))
    for j in range(5):
        final_target_train.append(train_y[i])

len(final_target_train), len(final_train_data)
final_train = np.array(final_train_data)
final_target_train = np.array(final_target_train)

fig,ax = plt.subplots(nrows=1,ncols=5,figsize=(20,20))
for i in range(5):
    ax[i].imshow(final_train[i+30])
    ax[i].axis('off')
plt.waitforbuttonpress()
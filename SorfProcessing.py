import numpy as np
import cv2
import CalcSorfResponse as calc
import matplotlib.pyplot as plt


class SorfProcessingClass():
    def __init__(self, image, mask=None,debug=True):
        '''SORF parameters'''
        plt.close('all')
        self.image = np.array(image)
        self.AlgParams = calc.AlgParamsSet(image)
        self.LocalSize = self.AlgParams.LocalSize;  # size of local area (squer area of sizeXsize pixels)
        self.LocalDecayCoef = self.AlgParams.LocalDecayCoef;  # decay coefficient of filter mask in local area
        self.RemoteSize = self.AlgParams.RemoteSize;  # size of Remote area (squer area of sizeXsize pixels)
        self.RemoteDecayCoef = self.AlgParams.RemoteDecayCoef;  # decay coefficient of filter mask in Remote area
        self.result = []
        self.contours = []
        self.contours1 = []
        self.border = []
        self.contours_comp = []
        self.border_comp = []
        self.mask = np.ones(self.image.size)
        self.debug = True
        if mask is not None:
            self.mask = mask

    # combine different resolutions
    def CombineRes(self, SorfResponses, NRes, Power):
        # MultiResResp = mean(abs(Responses).^Power,3);
        # MultiResResp = MultiResResp.^(1/Power);
        MultiResResp = [np.zeros([np.array(SorfResponses).shape[1],np.array(SorfResponses).shape[2]])]
        for ResolInd in range(0,NRes):
            # response for current resolution
            SorfResp = SorfResponses[ResolInd]

            # update multi resolution response
            MultiResResp = MultiResResp + np.sign(SorfResp) * ((np.abs(SorfResp))** Power);

        # normalize response: divide by the number of resolutions, take sqrt
        # and put the sign of the response)
        MultiResResp = MultiResResp / NRes;
        return np.sign(MultiResResp) * ((np.abs(MultiResResp))**(1 / Power));


    def SorfProcessing(self, TypeOfImg, TypeOfCenFltr, TypeOfSrndFltr,
                       TypeOfOperation,sorfTh=0, varargin=[]):
        # The algorithm is based on the paper "Brightness contrast�contrast
        # induction model predicts assimilation and inverted assimilation effects",
        # by Y. Barken, H. Spitzer and S. Einav, 2008.

        # Input parameters:
        #    AlgParams - algorithm parameters
        #    TypeOfImg - 'gray', 'color', 'both'
        #    TypeOfCenFltr - 'avarage', 'gaussian'
        #    TypeOfSrndFltr - 'avarage', 'gaussian'
        #    TypeOfOperation - 'SORF', 'relative'
        #                                             'SORF' -           center response minus surround response
        #                                             'relative' - center divided by surround
        #    varargin - first parameter: input color or gray image, that will be used instead of
        #                             the Elasto or B-mode images in AlgParams
        #                           second parameter: ceter sizes that are different from default center sizes
        #
        # Output parameters:
        #    [MultiResSorfRespElasto]
        #    or
        #    [MultiResSorfRespElasto MultiResSorfRespBmode]
        #    or
        #    [MultiResSorfRespElasto MultiResSorfRespBmode LocalContrast]
        #    or
        #    [MultiResSorfRespElasto MultiResSorfRespBmode LocalContrast RemoteContrast]

        # exponent mask for local contrast calculation
        LocalExponent = calc.CalcFieldMask(self.LocalSize, self.LocalDecayCoef, 0, TypeOfCenFltr)

        # exponent mask for remote contrast calculation
        RemoteExponent = calc.CalcFieldMask(self.RemoteSize,self.RemoteDecayCoef,self.LocalSize,TypeOfCenFltr)

        # images height and width (B-mode and elasto images have the same sizes)
        ImgHeight = self.image.shape[0]
        ImgWidth = self.image.shape[1]

        if (str(TypeOfImg)=="gray" or str(TypeOfImg)=='both'):
            # set color flag to gray
            self.AlgParams.ColorImageFlag = 0;

        # set parameters
        sizeOf = np.size(np.array(varargin))
        if(sizeOf == 0):# default image is used (B-mode image)
            InputImg = self.AlgParams.InputImgBmode
            CenSizes = self.AlgParams.CenSizes.Bmode
            OriginalImg = self.AlgParams.InputImgBmode
            InputTitle = ''
        elif(sizeOf == 1):#inputed image is used (enhanced B-mode)
            InputImg = varargin[1]
            CenSizes = self.AlgParams.CenSizes.Bmode
            OriginalImg = self.AlgParams.InputImgBmode
            InputTitle = ''
        elif(sizeOf == 2): #inputed image is used (intensity of Elasto image)
            InputImg = varargin[1]
            CenSizes = varargin[2]
            OriginalImg = self.AlgParams.InputImgEla
            InputTitle = ''
        elif(sizeOf ==3):#inputed image is used (intensity of Elasto image)
            InputImg = varargin[1]
            CenSizes = varargin[2]
            OriginalImg = self.AlgParams.InputImgElasto
            InputTitle = varargin[3]

        NumOfResolBmode = len(CenSizes)

        #initialize parameters for SORF responses for different resolutions
        SorfResponsesBmode = [np.zeros([ImgHeight,ImgWidth])]*NumOfResolBmode

        # initialize matrix for local contrast parameter
        LocalContrast = np.zeros([ImgHeight,ImgWidth])

        # initialize matrix for remote contrast parameter
        RemoteContrast = np.zeros([ImgHeight,ImgWidth])

        # loop over all rsolutions for B-mode image

        for ResolInd in range(0,NumOfResolBmode):
            CenSize = CenSizes[ResolInd];

            # SORF response for current resolution
            SorfResponseBmode = calc.CalcSorfResponse(self.AlgParams,InputImg,CenSize,TypeOfCenFltr,TypeOfSrndFltr,TypeOfOperation)

            # update SORF responses parameter
            SorfResponsesBmode[ResolInd] = SorfResponseBmode

            # calculate contrast for current resolution
            CurrentContrast = cv2.filter2D(abs(SorfResponseBmode) ** 2,cv2.CV_64F,  LocalExponent) / cv2.filter2D(
                abs(SorfResponseBmode) ** 1,cv2.CV_64F, LocalExponent);
    
            # update local contrast parameter
            LocalContrast = LocalContrast + CurrentContrast;

        if self.AlgParams.ShowBmodeRes:
            fig, ax = plt.subplots(3, 3)
            fig1, ax2 = plt.subplots(3, 3)
            fig2, ax3 = plt.subplots(3, 3)
            j = 0
            for ResolInd in range(0,NumOfResolBmode):
                explor = np.array(SorfResponsesBmode[ResolInd] < 0).astype(
                    np.uint8)

                image = cv2.normalize(src=SorfResponsesBmode[ResolInd], dst=None, alpha=0, beta=255,
                                      norm_type=cv2.NORM_MINMAX,
                                      dtype=cv2.CV_8U)

                contours = cv2.findContours(explor*255,
                                            cv2.RETR_TREE,
                                            cv2.CHAIN_APPROX_NONE)
                image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
                image = cv2.drawContours(image, contours[0], -1, (0, 0, 255),
                                       thickness=2)
                ax[j][ResolInd % 3].imshow(SorfResponsesBmode[ResolInd])
                ax2[j][ResolInd % 3].imshow(LocalContrast)
                ax3[j][ResolInd%3].imshow(image)
                if ResolInd % 3 == 2:
                    j += 1
            fig.canvas.set_window_title("ContrastPyr")
            fig.show()
            fig1.canvas.set_window_title("LocalContrast")
            fig1.show()
            fig1.canvas.set_window_title("Counter")
            fig2.show()
         #for ResolInd = 1:NumOfResolBmode
    # normalize LocalContrast to the range [0 1]
    #LocalContrast = LocalContrast/max(LocalContrast(:));


    # combine SORF responses from all the resolutions for B-mode image
        PowerBmode = self.AlgParams.MultiResPowerBmode
        MultiResSorfRespBmode = self.CombineRes(SorfResponsesBmode,NumOfResolBmode,PowerBmode)

        image = cv2.normalize(MultiResSorfRespBmode[0], dst=None,
                              alpha=0, beta=255,
                              norm_type=cv2.NORM_MINMAX,
                              dtype=cv2.CV_8U)
        val = 2
        val_image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        next = 0
        while(next):
            print(val)
            explor = np.array(MultiResSorfRespBmode[0] > val).astype(
                np.uint8)

            contours = cv2.findContours(explor * 255,
                                        cv2.RETR_TREE,
                                        cv2.CHAIN_APPROX_NONE)
            self.contours = cv2.drawContours(val_image, contours[0], -1, (0, 0, 255),
                                     thickness=2)
            self.contours = cv2.bitwise_and(self.contours,self.contours,
                                            mask = self.mask)
            cv2.imshow("new", self.contours)
            k = cv2.waitKey(0)
            if (k==49):
                val+=0.1
            if (k==50):
                val-=0.1
            if (k==51):
                next=0
            val_image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

        explor = np.array(MultiResSorfRespBmode[0] > sorfTh).astype(
            np.uint8)

        explor_compleat = np.array((MultiResSorfRespBmode[0] < 0) &
                                    (MultiResSorfRespBmode[0] > -1)).astype(
            np.uint8)

        contours  = cv2.findContours(explor * 255,
                                    cv2.RETR_TREE,
                                    cv2.CHAIN_APPROX_NONE)
        self.contours = cv2.drawContours(val_image, contours[0], -1,
                                         (0, 0, 255),
                                         thickness=2)
        self.contours1 = self.contours
        contours1 = cv2.findContours(explor_compleat * 255,
                                    cv2.RETR_TREE,
                                    cv2.CHAIN_APPROX_NONE)
        #self.contours_comp = cv2.drawContours(val_image, contours1[0], -1,
        #                                 (0, 255, 0),
        #                                 thickness=2)

        self.result = MultiResSorfRespBmode
        self.border = contours
        self.border_comp = contours1
        if self.AlgParams.ShowBmodeRes:
            plt.figure()
            plt.imshow(image)
            plt.show()

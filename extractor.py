# The extractor purpess is to extract a feature from the IR.
# The overall allgoritham is as follow:
# 1) Get an image.
# 2)

import cv2
import matplotlib.pyplot as plt
import time
from collections import Counter
import numpy as np

from numpy import uint8

from imageReader import nameExtractor
from skimage.transform import hough_circle, hough_circle_peaks
from scipy import ndimage
import skimage.graph
import os
from utill import saveResult

def draw_circle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        print(x, y)

class parabola():
    def __init__(self, x0=0, y0=0):
        self.x0 = x0
        self.y0 = y0
        self.point = 0
        self.maskParabola = []

class extractor():
    def __init__(self, imageIR, image, path, typeString="uint8", debug=False, saveResult=True):
        # Getting the input image
        self.typeString = typeString
        self.imageIR = imageIR
        self.image = cv2.normalize(image, dst=None, alpha=0, beta=255,
                                   norm_type=cv2.NORM_MINMAX).astype(self.typeString)
        self.imageHDR = image

        self.path = path
        self.debug = debug
        self.saveResult = saveResult
        self.rigthPoly = []
        self.leftPoly = []
        self.Point={0:[],1:[]}
        self.parabola = [parabola(),parabola()]
        self.fullPathMemory = ".\\Stage2Loction\\ROI\\memory"
        self.fullPathImage = ".\\Stage2Loction\\ROI\\image"

    def showImage(self,img):
        cv2.destroyAllWindows()
        cv2.imshow("new", img)
        cv2.waitKey(0)

    def activeShowImage(self,img):
        cv2.destroyAllWindows()
        cv2.imshow("new", img)
        return cv2.waitKey(0)

    def roots(self,matrix):
        b_v = matrix[:,1]
        a_v = matrix[:,0]
        c_v = matrix[:, 2]
        val = np.sqrt(b_v**2-4*a_v*c_v)/2*a_v
        val1 = (-1/b_v)/2*a_v+val
        val2 = (-1/b_v)/2*a_v-val
        return val1.astype(int),val2.astype(int)

    def houghparabola(self,BW, radius=3):

        ekernel_size = (radius, radius)
        kernel = np.ones(ekernel_size, np.uint8)
        BW[(BW>0)]=255
        div = 2        # resize image
        # PolyMask contains the polynom image
        polyImage = [None]*div
        # PolyMask contain the mask over the parbola
        polyMask = [None] * div


        for side in range(0,div):
            E = BW
            beginX = int((BW.shape[1] / div) * side)
            endX = int((BW.shape[1] / div) + (BW.shape[1] / div) * side)
            E = (E)[:, beginX: endX]
            Index = np.argwhere(E==255)
            rvals = np.arange(0.01,0.2,0.01)
            Idy = np.array(Index[:, 0]).reshape(Index[:, 0].size,1)
            Idx = np.array(Index[:, 1]).reshape(Index[:, 0].size,1)
            [N, M] = np.array(E).shape
            R = rvals.size
            ACC_MAT = np.zeros((N,M,R),np.uint8)
            x0_v = np.arange(0,  M,1)
            t = time.time()
            for r in range(0,R):
                for x0 in x0_v:
                    Y0 = Idy + rvals[r]*(Idx-x0)**2
                    Y0 = Y0[(Y0>250) & (Y0<N)].astype(int)
                    Y0_count = Counter(Y0)
                    for y0 in Y0_count:
                        ACC_MAT[y0,x0,r]+=Y0_count[y0]
            print(time.time() - t)
            if(np.amax(ACC_MAT) < 2):
                exit(-1)
            maxValue = np.argwhere(np.amax(ACC_MAT)==ACC_MAT)
            y0_v_max = maxValue[:, 0]
            x0_v_max = maxValue[:, 1]
            r0_v_max = maxValue[:, 2]
            print(r0_v_max)
            matrix = np.zeros((E.shape[0],E.shape[1],3)).astype(int)
            matrix1 = np.zeros((E.shape[0], E.shape[1])).astype(int)
            x_v = np.array(range(0, M)).reshape(M,1)
            count = 1

            for y0 in y0_v_max:
                for x0 in x0_v_max:
                    for r0 in r0_v_max:
                        y_v=np.round(y0 - rvals[r0] * (x_v - x0) ** 2)
                        point1 = np.concatenate((x_v, y_v), axis=1).astype(
                            int)

                        point1 = point1[(point1[:, 0] >= 0) & (point1[:, 0] < N)]
                        self.parabola[side].x0 = x0
                        self.parabola[side].y0 = x0
                        self.parabola[side].point = point1

                        matrix = cv2.circle(matrix, (x0, y0), 5,
                                            count, 1)
                        cv2.polylines(matrix, [point1.astype(int)], True,
                                        (count,0,0))
                        cv2.fillPoly(matrix1, [point1.astype(int)],
                                     color=[1])
                        count += 1
            matrix1 = cv2.dilate(matrix1.astype(uint8), kernel, iterations=1)
            polyImage[side] = matrix[:,:,0]
            polyMask[side] = matrix1
            self.parabola[side].maskParabola = polyMask[side]
        grayRGB = np.zeros([BW.shape[0], BW.shape[1], 3])
        grayRGB[:, :, 0] = BW
        grayRGB[:, :, 1] = BW
        grayRGB[:, :, 2] = BW
        maskVal = np.concatenate((polyImage[0], polyImage[1]), axis=1)
        #maskVal = np.pad(maskVal, [(0, 0), (1, 1)]).astype(
         #   uint8)
        maskValIntNot = np.bitwise_not(maskVal.astype(bool)).astype(uint8)
        grayRGB[:, :, 1] = grayRGB[:, :, 1] * maskValIntNot
        grayRGB[:, :, 2] = grayRGB[:, :, 2] * maskValIntNot
        grayRGB[:, :, 0] = grayRGB[:, :, 0] * maskValIntNot
        # rgb 001 010 100
        maskVal = maskVal.astype(uint8)
        grayRGB[:, :, 0] += (maskVal.astype(uint8)%2==1)*255
        maskVal = np.right_shift(maskVal,2)
        grayRGB[:, :, 1] += ((maskVal.astype(uint8) % 2) == 1)*255
        maskVal = np.right_shift(maskVal, 2)
        grayRGB[:, :, 2] += ((maskVal.astype(uint8) % 2) == 1)*255
        print("ok")
        if self.debug:
            plt.imshow(grayRGB)
            plt.waitforbuttonpress()
        return polyMask[0],polyMask[1],polyImage[0],polyImage[1]

    def findNippleAndCrop(self, suspection, image=None):
        if image is None:
            img = np.copy(self.image)
            zerosImage = np.zeros(self.image.shape)
        else:
            img = np.copy(image)
            zerosImage = np.zeros(image.shape)

        image1 = cv2.drawContours(zerosImage, suspection[0], -1, (255, 255,
                                                           255),
                                  thickness=1)
        #image1 = cv2.Canny(image1.astype(np.uint8()),20,100)
        hough_radii = np.arange(5, 20, 1)
        hough_res = hough_circle(image1, hough_radii)

        # Select the most prominent 3 circles
        accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                                   total_num_peaks=1)
        for (x, y, r) in zip(cx,  cy, radii):
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            image1 = cv2.circle(img, (x, y), r, (0, 255, 0), 4)
            crop = image1[x-r:x+r,y-r:y+r]
        valRetrun = None

        if(self.debug):
            self.showImage(image1)
            valRetrun = self.activeShowImage(img)

        if crop.size == 0:
            return None,valRetrun
        return crop,valRetrun

    # ABC: area between two curves
    def ABC(self, polly, BW, side):
        BW[0:100,:] = 0
        ekernel_size = (3, 3)
        kernel = np.ones(ekernel_size, np.uint8)
        beginX = int((self.image.shape[1] / 2) * side)
        endX = int((self.image.shape[1] / 2) + beginX)
        E = (BW)[:, beginX: endX]
        matrix = np.zeros((E.shape[0],E.shape[1])).astype(int)
        cv2.polylines(matrix, [self.parabola[side].point], False, [255, 0, 0], 1)
        intersection = np.argwhere(np.bitwise_and(E == 255, matrix == 255))
        intersection = intersection[intersection[:,1].argsort()]
        distanceX = intersection[:,1]-self.parabola[side].x0
        h, w = E.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)
        if side == 0:
            fun = np.min
        else:
            fun = np.max
        if np.size(np.where(distanceX < 0)) > 1:
            P1 = intersection[fun(np.array(np.where(distanceX < 0)))]
        else:
            return mask[0:h, 0:w]
        if np.size(np.where(distanceX > 0)) > 1:
            P2 = intersection[fun(np.array(np.where(distanceX > 0)))]
        else:
            return mask[0:h,0:w]
        array = np.bitwise_and(E == 255,np.not_equal(self.parabola[side].maskParabola,1))
        costs = np.where(array, 1, 1000)
        path, cost = skimage.graph.route_through_array(
            costs, start=P1,end=P2, fully_connected=True)
        matrix = np.zeros((E.shape[0], E.shape[1])).astype(int)
        temp = np.array(path)
        temp[:, [0, 1]] = temp[:, [1, 0]]
        cv2.polylines(matrix, [temp], False, [255, 0, 0], 1)
        image_rgb = cv2.cvtColor(E, cv2.COLOR_GRAY2RGB)
        image_rgb[:, :, 0] = np.array([polly > 0]).astype(int)[0, :, :] * 255
        image_rgb[:, :, 1] = np.array([polly > 0]).astype(int)[0, :, :] * 255
        image_rgb[:, :, 1] = matrix
        th, im_floodfill = cv2.threshold(cv2.cvtColor(image_rgb, cv2.COLOR_BGR2GRAY), 1, 255, cv2.THRESH_BINARY)
        middle = temp[temp[:, 0].argsort()]
        length = temp[temp[:, 0].argsort()].shape[0]
        Px = middle[middle[:,0]==middle[int(length / 2)][0]]
        Py = np.max(Px)
        cv2.floodFill(im_floodfill, mask, (Px[0,0], Py-3), 255)
        mask = cv2.dilate(mask.astype(uint8), kernel, iterations=1)

        if (self.debug):
            if side == 0:
                plt.suptitle("Right")
            else:
                plt.suptitle("Left")

            plt.imshow(image_rgb)

            plt.waitforbuttonpress()

        return np.bitwise_or(mask[0:h,0:w],(matrix>0).astype(uint8))

    def cropImage(self,mask,side):
        if (self.debug):
            cv2.imshow("new", np.uint8(mask))
        rawToSave = self.imageIR
        beginX = int((self.image.shape[1] / 2) * side)
        endX = int((self.image.shape[1] / 2) + (self.image.shape[1] / 2) *
                   side)
        E = (self.image)[:, beginX: endX]
        rawToSave = (rawToSave)[:, beginX: endX]
        val = np.argwhere(mask==255)
        x = val[:,1]
        y = val[:, 0]
        maxY =np.max(y)
        minY = np.min(y)
        maxX =np.max(x)
        minX = np.min(x)
        image = E[minY:maxY,minX:maxX]
        self.Point[side]=[minY,maxY,minX+beginX,maxX+beginX]
        rawToSave = rawToSave[minY:maxY,minX:maxX]
        if self.debug:
            self.showImage(image)
        name,state=nameExtractor(self.path)
        plt.gray()
        fig = plt.figure()

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        #plt.suptitle(name + state + str(side) + ".jpg", fontsize=20)
        if (self.debug):
            plt.imshow(image)
        #plt.imsave("./extractor/" + name + state + str(side) + "side.jpg", image, cmap='gray')
        cv2.imwrite("./extractor/" + name + state + str(side) + "side.jpg", image)
        #fig.savefig("./extractor/" + name + state + str(side) + "side.jpg",transparent = True, bbox_inches = 'tight', pad_inches = 0)
        np.savetxt("./memory/" + name + state + str(side) + "side.mem",
                   rawToSave,fmt='%.2f')
        return image

    def create_circle_mask(self,radius):
        diameter = 2 * radius + 1
        center = (radius, radius)
        mask = np.zeros((diameter, diameter), dtype=np.uint8)
        cv2.circle(mask, center, radius, 255, -1)
        return mask
    def connected_component_label2(self):
        # Mask creation
        # Type
        typeOfRun = "fast"
        ret, thresh = cv2.threshold(self.image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        laplacian = cv2.Canny(self.image, 0, 150) + thresh
        thresh = ndimage.binary_fill_holes(laplacian).astype(uint8)
        if (self.debug):
            plt.imshow(thresh, cmap="gray")
            plt.waitforbuttonpress()

        laplacian = cv2.Canny(self.image, 0 , 30)
        if (self.debug):
            plt.imshow(laplacian, cmap="gray")
            plt.waitforbuttonpress()

        self.rigthMask_Parabola, self.leftMask_Parabola, self.rigthPoly, self.leftPoly = self.houghparabola(laplacian,10)



        if typeOfRun == "full":
            # S1 creation
            S1_R = np.bitwise_and(self.rigthMask_Parabola > 0, thresh[:, 0:320])
            S1_L = np.bitwise_and(self.leftMask_Parabola > 0, thresh[:, 320:640])


            laplacian = cv2.Canny(self.image, 15, 40)
            S2_R = self.ABC(self.rigthMask_Parabola, laplacian, 0)
            S2_L = self.ABC(self.leftMask_Parabola, laplacian, 1)
            if(np.sum(S2_L)<20):
                S2_L = self.ABC(self.leftMask_Parabola, cv2.Canny(self.image, 0, 30), 1)

            if(np.sum(S1_R)>np.sum(S2_R)):
                maskR = S1_R | S2_R
            else:
                maskR = S1_R

            if (np.sum(S1_L) > np.sum(S2_L)):
                maskL = S1_L | S2_L
            else:
                maskL = S1_L
        else:
            S1_R = thresh[:, 0:320]
            S1_L = thresh[:, 320:640]

            maskR = S1_R
            maskL = S1_L
        mask = self.create_circle_mask(radius=5)
        mask2 = self.create_circle_mask(radius=8)

        maskR = cv2.dilate(maskR, mask)
        maskL = cv2.dilate(maskL, mask)

        maskR = cv2.erode(maskR, mask2)
        maskL = cv2.erode(maskL, mask2)



        masked_imageR = np.copy(self.image[:, 0:320])
        masked_imageR[maskR == 0] = 0
        masked_imageL = np.copy(self.image[:, 320:640])
        masked_imageL[maskL == 0] = 0
        imageL = np.copy(self.image[:, 0:320])
        imageR = np.copy(self.image[:, 320:640])

        if (self.debug):
            fig, ax = plt.subplots(4, 2)
            ax[0][0].set_title("Right Mask")
            ax[0][0].imshow(maskR, cmap='gray')
            ax[0][1].set_title("Left Mask")
            ax[0][1].imshow(maskL, cmap='gray')
            ax[1][0].set_title("Right Mask")
            ax[1][0].imshow(S1_R, cmap='gray')
            ax[1][1].set_title("Left Mask")
            ax[1][1].imshow(S1_L, cmap='gray')
            ax[2][0].set_title("Right Mask")
            ax[2][0].imshow(S2_R, cmap='gray')
            ax[2][1].set_title("Left Mask")
            ax[2][1].imshow(S2_L, cmap='gray')
            ax[3][0].set_title("Right Cup")
            ax[3][0].imshow(masked_imageR, cmap='gray')
            ax[3][1].set_title("Left Cup")
            ax[3][1].imshow(masked_imageL, cmap='gray')
            fig.show()
            plt.waitforbuttonpress()
            plt.close('all')

        if(self.saveResult):
            #masked_imageR, maskR, masked_imageL, maskL
            #fullPathMemory = "Stage2Loction\\ROI\\memory"
            name = str(self.path).split("\\")
            fullPath = os.getcwd()
            imageToSave = []
            imageToSave.append(masked_imageR)
            imageToSave.append(maskR)
            imageToSave.append(masked_imageL)
            imageToSave.append(maskL)
            saveResult(imageToSave,
                       os.path.join(fullPath, self.fullPathMemory, name[-4], name[-3], "containerR" + name[-1]),
                       "npy")

            # saveResult(masked_imageR,
            #            os.path.join(fullPath, self.fullPathImage, name[-4], name[-3], "masked_imageR" + name[-1]))
            # saveResult(masked_imageL,
            #            os.path.join(fullPath, self.fullPathImage, name[-4], name[-3], "masked_imageL" + name[-1]))
            # saveResult(maskR*255,
            #            os.path.join(fullPath, self.fullPathImage, name[-4], name[-3], "maskR" + name[-1]))
            # saveResult(maskL*255,
            #            os.path.join(fullPath, self.fullPathImage, name[-4], name[-3], "maskL" + name[-1]))

        body = np.copy(self.image)
        body[thresh == 0] = 0

        # todo: Move the wotk from connected_component_label to connected_component_label2
        return body, thresh, masked_imageR, maskR, masked_imageL, maskL, imageL, imageR

    def connected_component_label(self, path):

        # Converting those pixels with values 1-127 to 0 and others to 1
        # Applying cv2.connectedComponents()
        kernelSizes = [(3, 3), (9, 9), (15, 15)]
        #blurred = cv2.blur(self.image, (25, 25))
        #laplacian = cv2.Canny(self.image, 0, 150)
        # with open('test.npy', 'wb') as f:
        #     np.save(f, np.array(self.image))
        ret, thresh = cv2.threshold(self.image,0,255,cv2.THRESH_BINARY|cv2.THRESH_OTSU)
        laplacian = cv2.Canny(self.image, 0, 150)  + thresh
        thresh = ndimage.binary_fill_holes(laplacian).astype(uint8)
        if(self.debug):
            plt.imshow(thresh,cmap="gray")
            plt.waitforbuttonpress()

        num_labels, labels = cv2.connectedComponentsWithAlgorithm(thresh,
                                                     connectivity=8,ltype=cv2.CV_16U,labels=2,ccltype=cv2.CCL_DEFAULT)




        # set bg label to black

        labelsName = np.unique(labels)
        size = [0]*labelsName.size
        for label in labelsName:
            if label==0:
                continue
            size[label]= np.sum(labels[labels==label])/label
        correct_label = np.argwhere(np.max(size)==size)
        labels[labels != correct_label] = 0
        labels[labels == correct_label] = 1
        labels = np.array(labels).astype(np.uint8())
        #self.showImage(labels * 255)
        h, w = labels.shape[:2]
        for i in range(1,h):
            end = w - list(labels[i])[::-1].index(1)
            begin = list(labels[i]).index(1)
            labels[i][begin:end] = 1
        labels = np.array(labels).astype(np.uint8())
        index = 0

        while(index):
            laplacian = cv2.Canny(self.image,140,index)
            cv2.imshow("new", laplacian)
            k = cv2.waitKey(0)
            print(index)
            if (k==49):
                index+=1
            if (k==50):
                index-=1
            if (k==51):
                next=0
        result = cv2.bitwise_and(self.image,self.image, mask=labels)
        self.image = result

        laplacian = cv2.Canny(self.image, 140, 190)
        self.rigth, self.left, self.rigthPoly, self.leftPoly = self.houghparabola(laplacian)


        rigthIm = self.cropImage2(self.rigth,0)
        leftIm = self.cropImage(self.left, 1)
        print("ok")
        if (self.debug):
            fig, ax = plt.subplots(1, 2)
            ax[1].set_title("right")
            ax[1].imshow(leftIm, cmap='gray')
            ax[0].set_title("left")
            ax[0].imshow(rigthIm, cmap='gray')
            fig.show()
            plt.waitforbuttonpress()
            plt.close('all')


        resultR = result[self.Point[0][0]:self.Point[0][1],
                             self.Point[0][2]:self.Point[0][3]]
        labelsR = labels[self.Point[0][0]:self.Point[0][1],
                  self.Point[0][2]:self.Point[0][3]]
        resultL = result[self.Point[1][0]:self.Point[1][1], self.Point[1][
                                                                2]:self.Point[1][3]]
        labelsL = labels[self.Point[1][0]:self.Point[1][1],self.Point[1][
                                                               2]:self.Point[1][3]]
        imageL = self.imageHDR[self.Point[1][0]:self.Point[1][1], self.Point[1][
                                                                2]:self.Point[1][3]]
        imageR = self.imageHDR[self.Point[0][0]:self.Point[0][1],
                             self.Point[0][2]:self.Point[0][3]]


        return result,labels,resultR,labelsR,resultL,labelsL,imageL,imageR
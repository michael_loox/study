import os
import hrdLabs as hdr
from imageReader import imageReader
import matplotlib.pyplot as plt
import SorfProcessing as SP
import numpy as np
import cv2
from extractor import extractor
from imageReader import nameExtractor
import matplotlib.pyplot as plt

def showImage(img):
    # return
    cv2.destroyAllWindows()
    img = cv2.normalize(img, dst=None, alpha=0, beta=255,
                          norm_type=cv2.NORM_MINMAX,
                          dtype=cv2.CV_8U)
    cv2.imshow("new", img)
    cv2.waitKey(0)

class levelSet():
    def __init__(self,image):
        self.init = 0
        self.image = image

    def g_genertor(self):
        img = self.image
        kernel = cv2.getGaussianKernel(3,sigma=5)
        convImag = cv2.filter2D(img, -1,kernel)
        img = cv2.normalize(img, dst=None, alpha=0, beta=255,
                            norm_type=cv2.NORM_MINMAX,
                            dtype=cv2.CV_8U)
        ret, thresh = cv2.threshold(img,50,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        #cv2.imshow("",thresh)
        num_labels, labels = cv2.connectedComponents(thresh,
                                                     connectivity=8)
        labels = np.array(labels==1).astype(np.uint8())
        image=cv2.bitwise_and(img,img, mask=labels)
        [dx,dy]=np.gradient(image)
        image = np.sqrt(dx**2+dy**2)
        showImage(image)
        plt.close('all')
        plt.figure()
        #plt.hist(image, bins=50)
        plt.show()


def convertRawToImage(path):
        imageFileRaw = np.loadtxt(path)
        imageArray = imageFileRaw
        rgb_out = np.zeros(imageArray.shape, np.uint8)
        rgb_out = cv2.normalize(imageArray, rgb_out, 0, 255,
                                cv2.NORM_MINMAX).astype(np.uint8)
        return rgb_out

def main():
    files = []
    path = "memory"
    for r, d, f in os.walk(path):
        for file in f:
            files.append(os.path.join(r, file))

    for file in files:
        val, hdrRuner = hdr.runHdrAlgoritham(file, False)
        image = cv2.normalize(val.result, dst=None, alpha=0, beta=255,
                               norm_type=cv2.NORM_MINMAX,
                               dtype=cv2.CV_8U)
        image = convertRawToImage(file)
        levelSet(image).g_genertor()
        showImage(hdrRuner.imageValue)
        showImage(image)

if __name__ == "__main__":
    main()